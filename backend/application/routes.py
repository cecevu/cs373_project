from flask import current_app as app
from flask import jsonify, request
from .models import db, Team, Game, Player
from sqlalchemy import or_, and_

import json

team_search_cols = ['school_name', 'alt_name1', 'alt_name2', 'alt_name3']
game_search_cols = ["home_team", "away_team"]
player_search_cols = ["first_name", "last_name"]


"""
sorting and filtering modeled after previous years logic
gitlab here:
https://gitlab.com/10AMGroup11/bookrus/-/blob/main/backend/app.py
"""

def manual_paginate(query_list, page) :
    max_page = len(query_list) // 10
    if len(query_list) % 10 :
        max_page += 1
    
    if page > max_page :
        page = max_page

    return [query_list[i:i+10] for i in range(0, len(query_list), 10)][page - 1]

"""
endpoint for getting all teams by deafult, or a subset with parameters
filters:
    - City (this could change)
    - State
    - Conference
search teams:
    - some string or substring of the school name or alt names
sorting:
    - give a string that represents column to sort by (look in models.py for column names)
        (this is returned in ascending order by deafault)
page size = 10

"""
@app.route("/api/teams")
def all_teams():

    # begin query
    q = db.session.query(Team)

    # return list
    ql =[]

    # searching type bool
    multi_token = False

    # get all parameters
    city = request.args.get('city', type=str)
    state = request.args.get('state', type=str)
    conf = request.args.get('conf', type=str)
    search = request.args.get('search', type=str)
    sort = request.args.get('sort', type=str)
    page = request.args.get('page', type=int)

    # filter on explicit attributes
    if city is not None:
        q = q.filter(Team.city.ilike(city))
    if state is not None:
        q = q.filter(Team.state_name.ilike(state))
    if conf is not None:
        q = q.filter(Team.conference.ilike(conf))

    # filter by search terms
    if search is not None:
        search = search.split(" ")
        
        # find results with any of the substrings
        records = []
        for token in search :
            for col in team_search_cols :
                search_col = getattr(Team, col)
                records.append(search_col.ilike('%' + token + '%'))
        # any subs has any of the substrings in its result
        any_subs = q.filter(or_(*records))

        # if multiple search tokens we have to do some work!
        if len(search) > 1 :
            # find results with all of the substrings
            multi_token = True
            # all subs has ALL substrings in result
            all_subs = q.filter(and_ (or_ ((getattr(Team,scol)).ilike('%' + token + '%') for scol in team_search_cols) for token in search))

            all_sub_list = [record.as_dict() for record in all_subs.all()]
            any_sub_list = [record.as_dict() for record in any_subs.all() if record not in all_subs.all()]
            
            ql = all_sub_list + any_sub_list

            if page is not None:
               ql = manual_paginate(ql, page)
            return ql
        
        # else we can just let things be easy
        else :
            q = any_subs

    # sorting 
    if sort is not None and not multi_token:
        col_name = getattr(Team, sort)
        if col_name is not None:
            q = q.order_by(col_name)

    # pagination 
    if page is not None and not multi_token:
        q = q.paginate(page=page, per_page=10, error_out=False).items

    # return formatted results
    ql = [team.as_dict() for team in q]
    return ql



"""
endpoint for getting all games by deafult, or a subset with parameters
filters:
    - Home Team
    - Away Team
    - Season (pre/regular/post)
    - year
search teams:
    - some string or substring of the school name or alt names
sorting:
    - give a string that represents column to sort by (look in models.py for column names)
        (this is returned in ascending order by deafault)
page size = 10

"""
@app.route("/api/games")
def all_games():

    # begin query
    q = db.session.query(Game)

    # return list
    ql =[]

    # searching type bool
    multi_token = False

    # get all parameters
    home_team = request.args.get('hteam', type=str)
    away_team = request.args.get('ateam', type=str)
    season = request.args.get('season', type=int)
    search = request.args.get('search', type=str)
    sort = request.args.get('sort', type=str)
    page = request.args.get('page', type=int)

    # filter on explicit attributes
    if home_team is not None:
        q = q.filter(Game.home_team.ilike(home_team))
    if away_team is not None:
        q = q.filter(Game.away_team.ilike(away_team))
    if season is not None:
        q = q.filter(Game.season.ilike(season))

    # filter by search terms
    if search is not None:
        search = search.split(" ")
        
        # find results with any of the substrings
        records = []
        for token in search :
            for col in game_search_cols :
                search_col = getattr(Game, col)
                records.append(search_col.ilike('%' + token + '%'))
        # any subs has any of the substrings in its result
        any_subs = q.filter(or_(*records))

        # if multiple search tokens we have to do some work!
        if len(search) > 1 :
            # find results with all of the substrings
            multi_token = True
            # all subs has ALL substrings in result
            all_subs = q.filter(and_ (or_ ((getattr(Game,scol)).ilike('%' + token + '%') for scol in game_search_cols) for token in search))

            all_sub_list = [record.as_dict() for record in all_subs.all()]
            any_sub_list = [record.as_dict() for record in any_subs.all() if record not in all_subs.all()]

            ql = all_sub_list + any_sub_list

            if page is not None:
               ql = manual_paginate(ql, page)
            return ql
        
        # else we can just let things be easy
        else :
            q = any_subs

    # sorting 
    if sort is not None and not multi_token:
        col_name = getattr(Game, sort)
        if col_name is not None:
            q = q.order_by(col_name)

    # pagination 
    if page is not None and not multi_token:
        q = q.paginate(page=page, per_page=10, error_out=False).items

    # return formatted results
    ql = [game.as_dict() for game in q]
    return ql

"""
endpoint for getting all players by deafult, or a subset with parameters
filters:
    - Teams
    - Position
    - Conference
search teams:
    - first/last/team names
sorting:
    - give a string that represents column to sort by (look in models.py for column names)
        (this is returned in ascending order by deafault)
page size = 10

"""
@app.route("/api/players")
def all_players():

    # begin query
    q = db.session.query(Player)

    # return list
    ql =[]

    # searching type bool
    multi_token = False

    # get all parameters
    team_name = request.args.get('team', type=str)
    position = request.args.get('position', type=str)
    conf = request.args.get('conf', type=str)
    search = request.args.get('search', type=str)
    sort = request.args.get('sort', type=str)
    page = request.args.get('page', type=int)

    # filter on explicit attributes
    if team_name is not None:
        q = q.filter(Player.team.ilike(team_name))
    if position is not None:
        q = q.filter(Player.position.ilike(position))
    if conf is not None:
        q = q.filter(Player.conf.ilike(conf))

    # filter by search terms
    if search is not None:
        search = search.split(" ")
        
        # find results with any of the substrings
        records = []
        for token in search :
            for col in player_search_cols :
                search_col = getattr(Player, col)
                records.append(search_col.ilike('%' + token + '%'))
        # any subs has any of the substrings in its result
        any_subs = q.filter(or_(*records))

        # if multiple search tokens we have to do some work!
        if len(search) > 1 :
            # find results with all of the substrings
            multi_token = True
            # all subs has ALL substrings in result
            all_subs = q.filter(and_ (or_ ((getattr(Player,scol)).ilike('%' + token + '%') for scol in player_search_cols) for token in search))

            all_sub_list = [record.as_dict() for record in all_subs.all()]
            any_sub_list = [record.as_dict() for record in any_subs.all() if record not in all_subs.all()]
            
            ql = all_sub_list + any_sub_list

            if page is not None:
               ql = manual_paginate(ql, page)
            return ql
        
        # else we can just let things be easy
        else :
            q = any_subs

    # sorting 
    if sort is not None and not multi_token:
        col_name = getattr(Player, sort)
        if col_name is not None:
            q = q.order_by(col_name)

    # pagination 
    if page is not None and not multi_token:
        q = q.paginate(page=page, per_page=10, error_out=False).items

    # return formatted results
    ql = [game.as_dict() for game in q]
    return ql


@app.route("/api/teams/<int:team_id>")
def single_team(team_id):
    team = Team.query.filter_by(team_id=team_id)
    try:
        return team[0].as_dict()
    except:
        return f"Team {team_id} not found"


@app.route("/api/games/<int:game_id>")
def single_game(game_id):
    game = Game.query.filter_by(game_id=game_id)
    try:
        return game[0].as_dict()
    except:
        return f"Game {game_id} not found"


@app.route("/api/players/<int:player_id>")
def single_player(player_id):
    player = Player.query.filter_by(player_id=player_id)
    try:
        return player[0].as_dict()
    except:
        return f"player {player_id} not found"



