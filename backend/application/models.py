from . import (
    db,
)  # this is why we used the factory method in __init__, makes db accessable

# data model for players
class Player(db.Model):

    __tablename__ = "Players"

    player_id = db.Column(db.Integer, primary_key=True)
    photo1 = db.Column(db.String(255))
    photo2 = db.Column(db.String(255))
    season = db.Column(db.Integer)
    name = db.Column(db.String(255))
    position = db.Column(db.String(5))
    team = db.Column(db.String(255))
    conf = db.Column(db.String(25))
    overall = db.Column(db.Float)
    passing = db.Column(db.Float)
    rush = db.Column(db.Float)
    first_down = db.Column(db.Float)
    second_down = db.Column(db.Float)
    third_down = db.Column(db.Float)
    standard_downs = db.Column(db.Float)
    passing_downs = db.Column(db.Float)
    first_name = db.Column(db.String(255))
    last_name = db.Column(db.String(255))
    weight = db.Column(db.Integer)
    height = db.Column(db.Integer)
    jersey = db.Column(db.Integer)
    year = db.Column(db.Integer)
    home_city = db.Column(db.String(255))
    home_state = db.Column(db.String(10))
    home_country = db.Column(db.String(10))
    home_latitude = db.Column(db.String(25))
    home_longitude = db.Column(db.String(25))
    home_county_fips = db.Column(db.String(255))

    def as_dict(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}


# data model for games
class Game(db.Model):

    __tablename__ = "Games"

    game_id = db.Column(db.Integer, primary_key=True)
    season = db.Column(db.Integer)
    week = db.Column(db.Integer)
    season_type = db.Column(db.String(50))
    start_date = db.Column(db.String(50))
    start_time_tbd = db.Column(db.Boolean)
    neutral_site = db.Column(db.Boolean)
    conf_game = db.Column(db.Boolean)
    attendance = db.Column(db.Integer)
    venue_id = db.Column(db.Integer)
    venue = db.Column(db.String(50))
    home_id = db.Column(
        db.Integer
        # db.ForeignKey('Teams.team_id'),
        # nullable=False
    )
    # home_r = db.relationship(
    #     "Team",
    #     foreign_keys=[home_id]
    # )
    home_team = db.Column(db.String(255))
    home_conf = db.Column(db.String(255))
    home_div = db.Column(db.String(50))
    home_points = db.Column(db.Integer)
    home_win_prob = db.Column(db.Float)
    home_pre_elo = db.Column(db.Integer)
    home_post_elo = db.Column(db.Integer)
    away_id = db.Column(
        db.Integer
        # db.ForeignKey('Teams.team_id'),
        # nullable=False
    )
    # away_r = db.relationship(
    #     "Team",
    #     foreign_keys=[away_id]
    # )
    away_team = db.Column(db.String(255))
    away_conf = db.Column(db.String(255))
    away_div = db.Column(db.String(50))
    away_points = db.Column(db.Integer)
    away_win_prob = db.Column(db.Float)
    away_pre_elo = db.Column(db.Integer)
    away_post_elo = db.Column(db.Integer)
    excitement_index = db.Column(db.Float)
    video1 = db.Column(db.String(255))
    video2 = db.Column(db.String(255))

    def as_dict(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}


# data model for teams
class Team(db.Model):

    __tablename__ = "Teams"

    team_id = db.Column(db.Integer, primary_key=True)
    # games_played = db.relationship(
    #     'Game',
    #     backref='Teams',
    #     lazy=True
    # )
    school_name = db.Column(db.String(255))
    mascot = db.Column(db.String(255))
    abbreviation = db.Column(db.String(10))
    alt_name1 = db.Column(db.String(50))
    alt_name2 = db.Column(db.String(50))
    alt_name3 = db.Column(db.String(50))
    conference = db.Column(db.String(50))
    division = db.Column(db.String(50))
    color = db.Column(db.String(7))
    alt_color = db.Column(db.String(7))
    logos1 = db.Column(db.String(255))
    logos2 = db.Column(db.String(255))
    twitter = db.Column(db.String(255))
    venue_name = db.Column(db.String(255))
    city = db.Column(db.String(255))
    state_name = db.Column(db.String(255))
    zipcode = db.Column(db.Integer)
    country_code = db.Column(db.String(2))
    capacity = db.Column(db.Integer)
    grass = db.Column(db.Boolean)
    dome = db.Column(db.Boolean)

    def as_dict(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}

    # __rep__ is similar to __str__ but doesn't have to be human readable
    # can be used to quickly get the data of something for machine purposes
    def __rep__(self):
        # should hopefully return a formatted version of a team given by team_id
        # not sure if this really works, may delete later
        return "<Team {}>".format(self.team_id)
