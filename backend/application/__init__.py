from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_cors import CORS

# doing this allows db to be globally accessable, though it is instantiated
# inside the create_app() func below
# read more about structuring and why here:
# https://hackersandslackers.com/flask-application-factory
# this code is also taken form the above link
# essentially we are separating app.py into many files that all
# handle a specific task
db = SQLAlchemy()


def create_app(config_object):
    """Construct the core application."""
    # these two lines instantiate app, but call config.py for startup settings
    # this is lazy, so nothing is really done, just set up
    app = Flask(__name__, instance_relative_config=False)
    CORS(app)
    app.config.from_object(config_object)

    # now we initialize the database with our skeleton of app
    db.init_app(app)

    # from what I understand, app_context is everything that is real. basically if its not
    # inside context, it may as well not exist because it is unreachable as far as Flask cares
    with app.app_context():

        # this is the file with all the decorators functions that serve when endpoints are used
        from . import routes  # Import routes
        from . import db_routes # Import database builder routes, comment out in final build!

        # if this doesn't work, check this link for blueprinting
        # https://hackersandslackers.com/flask-blueprints
        # db.drop_all() # for debugging database stuff, REMOVE
        db.create_all()  # Create sql tables for our data models

        return app
