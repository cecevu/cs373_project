from flask import current_app as app
from flask import jsonify
from .models import db, Team, Game, Player

import json

"""
begin opening real data, should be only data eventually
"""
with open("data/teamData.json") as f:
    fteams = json.load(f)

with open("data/games2021.json") as f:
    g21 = json.load(f)

with open("data/games2022.json") as f:
    g22 = json.load(f)

with open("data/gameVideo2021.json") as f:
    gv21 = json.load(f)

with open("data/gameVideo2022.json") as f:
    gv22 = json.load(f)

with open("data/player2021.json") as f:
    p21 = json.load(f)

with open("data/player2022.json") as f:
    p22 = json.load(f)

with open("data/playerPhoto2021.json") as f:
    pp21 = json.load(f)

with open("data/playerPhoto2022.json") as f:
    pp22 = json.load(f)


# running any of the build commands again should give
# duplicate error, as database already has entries
@app.route("/build")
def build_all():
    build_teams()
    build_games()
    build_players()
    return "building database"


@app.route("/build/players")
def build_players():
    """
    structure of player2021 :
    dict{
        player_id : list [
            dict{
                some stats
                usage : list[
                    more stats
                ]
            },
            dict{more stats}
        ]
    }
    """
    # merge the years
    p = {**p21, **p22}

    # merge the pps ---- LOL
    pps = {}
    for pp in pp21:
        pps = {**pps, **pp}
    for pp in pp22:
        pps = {**pps, **pp}

    for pid in p:
        # get player
        player = p[pid]

        # attempt to get photos
        try:
            pp1 = pps[pid][0]
            pp2 = pps[pid][1]
        except:
            pass

        # build new record
        new_player_record = Player(
            player_id=int(player[0]["id"]),
            photo1=pp1,
            photo2=pp2,
            season=player[0]["season"],
            name=player[0]["name"],
            position=player[0]["position"],
            team=player[0]["team"],
            conf=player[0]["conference"],
            overall=player[0]["usage"]["overall"],
            passing=player[0]["usage"]["pass"],
            rush=player[0]["usage"]["rush"],
            first_down=player[0]["usage"]["firstDown"],
            second_down=player[0]["usage"]["secondDown"],
            third_down=player[0]["usage"]["thirdDown"],
            standard_downs=player[0]["usage"]["standardDowns"],
            passing_downs=player[0]["usage"]["passingDowns"],
            first_name=player[1]["first_name"],
            last_name=player[1]["last_name"],
            weight=player[1]["weight"],
            height=player[1]["height"],
            jersey=player[1]["jersey"],
            year=player[1]["year"],
            home_city=player[1]["home_city"],
            home_state=player[1]["home_state"],
            home_country=player[1]["home_country"],
            home_latitude=player[1]["home_latitude"],
            home_longitude=player[1]["home_longitude"],
            home_county_fips=player[1]["home_county_fips"],
        )

        db.session.add(new_player_record)

    db.session.commit()

    return "building players"


@app.route("/build/games")
def build_games():
    """
    note to self : games2021 is structured as follows:
    list[
        dict{ game_id : list[
            dict {useful}, dict {random stats}
        ]
        }
    ]
    """
    g = g21 + g22  # merge lists

    gvs = {}  # create a dict for all game videos
    for game in gv21:
        gvs = {**gvs, **game}
    for game in gv22:
        gvs = {**gvs, **game}

    for game_dict in g:
        # this is some awkward stuff due to the way the json is structured
        gid = ""
        for key in game_dict:
            gid = key
        game = game_dict[gid][0]  # get the useful stuff!

        # attempt to get vids
        try:
            gv1 = gvs[gid][0]
            gv2 = gvs[gid][1]
        except:
            pass

        new_game_record = Game(
            game_id=gid,
            season=game["season"],
            week=game["week"],
            season_type=game["season_type"],
            start_date=game["start_date"],
            start_time_tbd=game["start_time_tbd"],
            neutral_site=game["neutral_site"],
            conf_game=game["conference_game"],
            attendance=game["attendance"],
            venue_id=game["venue_id"],
            venue=game["venue"],
            home_id=game["home_id"],
            home_team=game["home_team"],
            home_conf=game["home_conference"],
            home_div=game["home_division"],
            home_points=game["home_points"],
            home_win_prob=game["home_post_win_prob"],
            home_pre_elo=game["home_pregame_elo"],
            home_post_elo=game["home_postgame_elo"],
            away_id=game["away_id"],
            away_team=game["away_team"],
            away_conf=game["away_conference"],
            away_div=game["away_division"],
            away_points=game["away_points"],
            away_win_prob=game["away_post_win_prob"],
            away_pre_elo=game["away_pregame_elo"],
            away_post_elo=game["away_postgame_elo"],
            excitement_index=game["excitement_index"],
            video1=gv1,
            video2=gv2,
        )

        db.session.add(new_game_record)

    db.session.commit()
    return "building games"


# this should probbly be written out to its own file eventually
@app.route("/build/teams")
def build_teams():

    for team in fteams:
        loc = team["location"]

        new_team_record = Team(
            team_id=team["id"],
            school_name=team["school"],
            mascot=team["mascot"],
            abbreviation=team["abbreviation"],
            alt_name1=team["alt_name1"],
            alt_name2=team["alt_name2"],
            alt_name3=team["alt_name3"],
            conference=team["conference"],
            division=team["division"],
            color=team["color"],
            alt_color=team["alt_color"],
            logos1=team["logos"][0],
            logos2=team["logos"][1],
            twitter=team["twitter"],
            venue_name=loc["name"],
            city=loc["city"],
            state_name=loc["state"],
            zipcode=loc["zip"],
            country_code=loc["country_code"],
            capacity=loc["capacity"],
            grass=loc["grass"],
            dome=loc["dome"],
        )

        db.session.add(new_team_record)

    db.session.commit()
    return "built teams"