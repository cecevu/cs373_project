from application import create_app, db, models

import operator
import unittest
import json

app = create_app("config.TestConfig")


def build_teams(db):
    with open("test_data/teams.json") as f:
        teams = json.load(f)["teams"]

    output = []
    for team in teams:
        new_team_record = models.Team(
            team_id=team["id"],
            school_name=team["name"],
            mascot=team["mascot"],
            abbreviation="",
            alt_name1="",
            alt_name2="",
            alt_name3="",
            conference=team["conference"],
            division="",
            color="",
            alt_color="",
            logos1="",
            logos2="",
            twitter="",
            venue_name=team["stadium"],
            city=team["location"],
            state_name="",
            zipcode=0,
            country_code="",
            capacity=0,
            grass=False,
            dome=False,
        )

        output.append(new_team_record.as_dict())
        db.session.add(new_team_record)
    db.session.commit()

    output.sort(key=operator.itemgetter("team_id"))
    return output


def build_games(db):
    with open("test_data/games.json") as f:
        games = json.load(f)["games"]

    output = []
    for game in games:
        new_game_record = models.Game(
            game_id=game["game_id"],
            season=game["game_year"],
            week=game["game_week"],
            season_type="",
            start_date="",
            start_time_tbd=False,
            neutral_site=False,
            conf_game=game["in_conference"],
            attendance=0,
            venue_id=0,
            venue=game["game_location"],
            home_id=0,
            home_team=game["home_team"],
            home_conf="",
            home_div="",
            home_points=game["home_team_points"],
            home_win_prob=game["home_team_win_chance"],
            home_pre_elo=0,
            home_post_elo=0,
            away_id=0,
            away_team=game["away_team"],
            away_conf="",
            away_div="",
            away_points=game["away_team_points"],
            away_win_prob=1 - game["home_team_win_chance"],
            away_pre_elo=0,
            away_post_elo=0,
            excitement_index=0.0,
            video1="",
            video2="",
        )

        output.append(new_game_record.as_dict())
        db.session.add(new_game_record)
    db.session.commit()

    output.sort(key=operator.itemgetter("game_id"))
    return output


def build_players(db):
    with open("test_data/players.json") as f:
        players = json.load(f)["players"]

    output = []
    for player in players:
        new_player_record = models.Player(
            player_id=player["player_id"],
            photo1="",
            photo2="",
            season=0,
            name=player["player_first_name"] + player["player_last_name"],
            position=player["position"],
            team=player["team_name"],
            conf="",
            overall=0.0,
            passing=0.0,
            rush=0.0,
            first_down=0.0,
            second_down=0.0,
            third_down=0.0,
            standard_downs=0.0,
            passing_downs=0.0,
            first_name=player["player_first_name"],
            last_name=player["player_last_name"],
            weight=player["player_weight"],
            height=player["player_height"],
            jersey=player["player_jersey"],
            year=0,
            home_city="",
            home_state="",
            home_country="",
            home_latitude="",
            home_longitude="",
            home_county_fips="",
        )

        output.append(new_player_record.as_dict())
        db.session.add(new_player_record)
    db.session.commit()

    output.sort(key=operator.itemgetter("player_id"))
    return output


class BackendTestCase(unittest.TestCase):
    @classmethod
    def setUpClass(self):
        self.ctx = app.app_context()
        self.ctx.push()
        self.client = app.test_client()
        with self.ctx:
            db.drop_all()
            db.create_all()
            self.teams = build_teams(db)
            self.games = build_games(db)
            self.players = build_players(db)

    @classmethod
    def tearDownClass(self):
        self.ctx.pop()
        with self.ctx:
            db.session.remove()
            db.drop_all()

    def test_all_teams(self):
        response = self.client.get("/api/teams")
        data = json.loads(response.data)
        data.sort(key=operator.itemgetter("team_id"))

        self.assertEqual(response.status_code, 200)
        self.assertEqual(data, self.teams)

    def test_single_team(self):
        response = self.client.get("/api/teams/1")
        data = json.loads(response.data)

        assert self.teams[0]["team_id"] == 1
        self.assertEqual(response.status_code, 200)
        self.assertEqual(data, self.teams[0])

    def test_all_games(self):
        response = self.client.get("/api/games")
        data = json.loads(response.data)
        data.sort(key=operator.itemgetter("game_id"))

        self.assertEqual(response.status_code, 200)
        self.assertEqual(data, self.games)

    def test_single_game(self):
        response = self.client.get("/api/games/178")
        data = json.loads(response.data)

        assert self.games[0]["game_id"] == 178
        self.assertEqual(response.status_code, 200)
        self.assertEqual(data, self.games[0])

    def test_all_players(self):
        response = self.client.get("/api/players")
        data = json.loads(response.data)
        data.sort(key=operator.itemgetter("player_id"))

        self.assertEqual(response.status_code, 200)
        self.assertEqual(data, self.players)

    def test_single_player(self):
        response = self.client.get("/api/players/196")
        data = json.loads(response.data)

        assert self.players[0]["player_id"] == 196
        self.assertEqual(response.status_code, 200)
        self.assertEqual(data, self.players[0])


if __name__ == "__main__":
    unittest.main()
