# TODO :
# figure out how to obscure the SQLDB_URI
# something to do with os/dotenv vars


class Config:
    # set to true if you want all query results to be output to stderror
    SQLALCHEMY_ECHO = False
    # this uses a ton of memory to track all changes, since we are read only, set to
    SQLALCHEMY_TRACK_MODIFICATIONS = False


class ProdConfig(Config):
    # this needs to be obscured, makes database vunerable
    SQLALCHEMY_DATABASE_URI = "mysql://admin:CS373ncaa8@ncaa-access-db.cbdnrdpxbn93.us-east-2.rds.amazonaws.com:3306/ncaa_db"


class TestConfig(Config):
    SQLALCHEMY_DATABASE_URI = "sqlite:///testing.db"
