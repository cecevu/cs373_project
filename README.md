Canvas/Discord Group Number: 10-8

Names of Team Members (UT eid, GitLabID): 
- Cece Vu: cqv66 cecevu
- Willard Bui: wpb425 willardbui
- Gus Caldwell: mac22334 skinnertx
- Bruno Fazzani: bf8526 bfazzani1

Git SHA: daf1f5c0b3e405f698ca07eefbe267d92a24a71f

Website: https://www.ncaaccess.me

Phase I

Project Leader: Cece Vu

GitLab Pipelines: https://gitlab.com/cecevu/cs373_project/-/pipelines

Estimated completion time for each member:
- Dheeraj Kashyap: 10
- Cece Vu: 10
- Willard Bui: 10
- Gus Caldwell: 10
- Bruno Fazzani: 10

Actual completion time for each member:
- Dheeraj Kashyap: 20
- Cece Vu: 20
- Willard Bui: 20
- Gus Caldwell: 20
- Bruno Fazzani: 15

Phase II

Project Leader: Gus Caldwell

GitLab Pipelines: https://gitlab.com/cecevu/cs373_project/-/pipelines

Estimated completion time for each member:
- Cece Vu: 15
- Willard Bui: 15
- Gus Caldwell: 15
- Bruno Fazzani: 15

Actual completion time for each member:
- Cece Vu: 21
- Willard Bui: 19
- Gus Caldwell: 22
- Bruno Fazzani: 18

Phase III

Project Leader: Willard Bui

GitLab Pipelines: https://gitlab.com/cecevu/cs373_project/-/pipelines

Estimated completion time for each member:
- Cece Vu: 10
- Willard Bui: 10
- Gus Caldwell: 10
- Bruno Fazzani: 10

Actual completion time for each member:
- Cece Vu: 10
- Willard Bui: 9
- Gus Caldwell: 10
- Bruno Fazzani: 9

Phase IV

Project Leader: Bruno Fazzani

GitLab Pipelines: https://gitlab.com/cecevu/cs373_project/-/pipelines

Estimated completion time for each member:
- Cece Vu: 10
- Willard Bui: 10
- Gus Caldwell: 10
- Bruno Fazzani: 10

Actual completion time for each member:
- Cece Vu: 10
- Willard Bui: 10
- Gus Caldwell: 10
- Bruno Fazzani: 10

Comments: We referenced [this](https://gitlab.com/cs373-11am-group12/stayfresh/-/blob/main/frontend/src/components/pages/About.js) for gitlab api