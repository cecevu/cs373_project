from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.chrome.options import Options
from selenium.webdriver import Remote
from selenium.webdriver.common.keys import Keys

import argparse
import time

# change the local host link to the link resulting from npm start
LOCAL_HOST = 'http://localhost:3000/'
WEBSITE_LINK = 'https://www.ncaaccess.me/'
PATH = './chromedriver'
PATH_LIN = './chromedriver_linux'

def initialize(local_host, CICD):
    chrome_options = Options()
    if CICD is not None and CICD:
        chrome_options.add_argument("--headless")
        chrome_options.add_argument("--window-size=1920,1080")
        chrome_options.add_argument("--no-sandbox")
        chrome_options.add_argument("--disable-dev-shm-usage")

    driver = webdriver.Chrome(service = Service(ChromeDriverManager().install()), options = chrome_options)


    driver.maximize_window()
    return driver


def get_page(local_host, driver):
    # Change between the links of the webpage vs the devlopment code
    if local_host:
        driver.get(LOCAL_HOST)
    else:
        driver.get(WEBSITE_LINK)
    return driver

# Test to go to the teams model page from the home page
def test0(local_host, driver):
    driver = get_page(local_host, driver)
    to_teams = driver.find_element(By.ID, "teams-btn")
    to_teams.click()
    # Sleep the page if wanting to view the page
    # time.sleep(1)
    return driver

# goes to the players model page from the home page
def test1(local_host, driver):
    driver = get_page(local_host, driver)
    to_players = driver.find_element(By.ID, "players-btn")
    to_players.click()
    # Sleep the page if wanting to view the page
    # time.sleep(1)
    return driver

# goes to the games model page from the home page
def test2(local_host, driver):
    driver = get_page(local_host, driver)
    to_games = driver.find_element(By.ID, "games-btn")
    to_games.click()
    # Sleep the page if wanting to view the page
    # time.sleep(1)
    return driver

# goes into stanford page from teams model page
def test3(local_host, driver):
    driver = get_page(local_host, driver)
    to_teams = driver.find_element(By.ID, "teams-btn")
    to_teams.click()
    WebDriverWait(driver, 20).until(EC.element_to_be_clickable((By.XPATH, "//a[@href='/teams/24']"))).click()
    return driver

# goes into Will Rogers page from the players model page
def test4(local_host, driver):
    driver = get_page(local_host, driver)
    to_players = driver.find_element(By.ID, "players-btn")
    to_players.click()
    WebDriverWait(driver, 20).until(EC.element_to_be_clickable((By.XPATH, "//a[@href='/players/102597']"))).click()
    # Sleep the page if wanting to view the page
    # time.sleep(1)
    return driver

# goes into Miami Alabama game instance page from the games model page
def test5(local_host, driver):
    driver = get_page(local_host, driver)
    to_games = driver.find_element(By.ID, "games-btn")
    to_games.click()
    WebDriverWait(driver, 20).until(EC.element_to_be_clickable((By.XPATH, "//a[@href='/games/401281942']"))).click()
    # Sleep the page if wanting to view the page
    # time.sleep(1)
    return driver

# goes to the players model page from the home page and then back to the home page using the nav bar
def test6(local_host, driver):
    driver = get_page(local_host, driver)
    to_players = driver.find_element(By.ID, "players-btn")
    to_players.click()
    # sleeping for visibility
    # time.sleep(2)
    WebDriverWait(driver, 20).until(EC.element_to_be_clickable((By.XPATH, "//a[@data-rr-ui-event-key='/']"))).click()
    # Sleep the page if wanting to view the page
    # time.sleep(1)
    return driver

# Testing clicking the navigation bar dropdown for the models tab
def test7(local_host, driver):
    driver = get_page(local_host, driver)
    models = driver.find_element(By.ID, "basic-nav-dropdown")
    models.click()
    # Sleep the page if wanting to view the page
    # time.sleep(1)
    return driver

# goes to the about page from the home page
def test8(local_host, driver):
    driver = get_page(local_host, driver)
    WebDriverWait(driver, 20).until(EC.element_to_be_clickable((By.XPATH, "//a[@data-rr-ui-event-key='/about']"))).click()
    # Sleep the page if wanting to view the page
    # time.sleep(1)
    return driver

# goes to the players model page from the home page and then back to the home page using the logo button
def test9(local_host, driver):
    driver = get_page(local_host, driver)
    to_players = driver.find_element(By.ID, "players-btn")
    to_players.click()
    time.sleep(2)
    WebDriverWait(driver, 20).until(EC.element_to_be_clickable((By.XPATH, "//a[@class='navbar-brand']"))).click()
    # Sleep the page if wanting to view the page
    # time.sleep(1)
    return driver

funcs = [test0, test1, test2, test3, test4, test5, test6, test7, test8, test9]

def runTests(start, end, local_host, CICD):
    assert end > start
    assert start >= 0 and start < len(funcs)
    assert end <= len(funcs)
    for i in range(start, end):
        test = funcs[i]
        driver = initialize(local_host, CICD)
        driver = test(local_host, driver)
        # Sleep the page if wanting to view all pages resulting from all tests
        # time.sleep(3)
        driver.quit()
    return


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("-s", "--start",
                                    dest= "start",
                                    type= int,
                                    required= False,
                                    help="The test number to start running tests at (0 indexed and inclusive).")
    parser.add_argument("-e", "--end",
                                    dest= "end",
                                    type= int,
                                    required= False,
                                    help="The test number to end running tests at (0 indexed and exclusive).")
    parser.add_argument("-l", "--local_hosting",
                                    dest= "local_host_bool",
                                    type= bool,
                                    required= False,
                                    help="Boolean to determine if the code is to be run off a local web page.")
    parser.add_argument("-c", "--CI",
                                    dest= "CICD",
                                    type= bool,
                                    required= False,
                                    help="Boolean to determine if the code is to be run off the CICD pipeline.")
    args = parser.parse_args()
    if args.local_host_bool is None:
        runTests(0 if args.start is None else args.start, len(funcs) if args.end is None else args.end, False, args.CICD)
    else:
        runTests(0 if args.start is None else args.start, len(funcs) if args.end is None else args.end, args.local_host_bool, args.CICD)
