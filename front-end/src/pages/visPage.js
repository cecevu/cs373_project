import React, {useState, useEffect} from "react";
import { BarChart, Bar, XAxis, YAxis, CartesianGrid, Tooltip, Legend, Scatter, ScatterChart, LineChart, Line, Label} from 'recharts';
import axios from "axios";

const PlayerStates = ({ players }) => {
    let stateCount = {};

    // loop thru all players and find size of data
    for (const player of players) {
        if (player.home_state) {
            let state = player.home_state;
            if (state in stateCount) {
                stateCount[state] += 1;
            } else {
                stateCount[state] = 1;
            }
        }
    }

    const data = [];

    for (var key in stateCount) {
        var value = stateCount[key];
        var treeDict = {"state": key, "players": value};
        data.push(treeDict);
    }

    data.sort((a, b) =>
        b["players"] - a["players"]
    );

    const CustomTooltip = ({ active, payload, label }) => {
        if (active && payload && payload.length) {
          return (
            <div className="custom-tooltip">
              <p className="label">{`${label} : ${payload[0].value}`}</p>
            </div>
          );
        }
      
        return null; 
      };

    return (
        <div>
        <h1 style={{
            textAlign: "center",
            fontWeight: "bold",
            fontSize: "34px"
        }}>Players by State</h1>
        <BarChart
            width={1800}
            height={600}
            data={data}
            margin={{
                top: 5,
                right: 10,
                left: 10,
                bottom: 5,
            }}
        >
            <CartesianGrid strokeDasharray="3 3" />
            <XAxis dataKey="state"/>
            <YAxis label={{value: "Players", angle: -90, position: "insideLeft"}}/>
            <Tooltip content={<CustomTooltip />} />
            <Bar dataKey="players" barSize={20} fill="#8884d8" />
        </BarChart>
        </div>
      );
};

const PlayerSizes = ({players}) => {
    const data = [];

    for (const player of players) {
        if (player.height && player.weight) {
            data.push({
                "x": player.weight,
                "y": player.height,
                "position": player.position
            });
        }
    }

    return (
    <div>
    <h1 style={{
        textAlign: "center",
        fontWeight: "bold",
        fontSize: "34px"
    }}>Player Size by Position</h1>
    <ScatterChart
        width={1200}
        height={600}
        margin={{
          top: 20,
          right: 20,
          bottom: 20,
          left: 20,
        }}
    >
        <CartesianGrid />
        <XAxis type="number" dataKey="x" name="weight" unit="lb" domain={['auto', 'auto']}/>
        <YAxis type="number" dataKey="y" name="height" unit="in" domain={['auto', 'auto']}/>
        <Legend/>
        <Tooltip cursor={{ strokeDasharray: '3 3' }} />
        <Scatter name="QB" data={data.filter((item) => item.position === "QB")} fill="#f56c42"/>
        <Scatter name="RB" data={data.filter((item) => item.position === "RB")} fill="#4dac26"/>
        <Scatter name="WR" data={data.filter((item) => item.position === "WR")} fill="#5e3c99"/>
        <Scatter name="TE" data={data.filter((item) => item.position === "TE")} fill="#3399c4"/>
        <Scatter name="FB" data={data.filter((item) => item.position === "FB")} fill="#c932e3"/>
    </ScatterChart>
    </div>);
};

const AttendanceByWeek = ({games}) => {
    const weeks = Array.from({length: 14}, () => ({}));
    const confs = new Set([
        "ACC",
        "American Athletic",
        "Big 12",
        "Big Ten",
        "Conference USA",
        "FBS Independents",
        "Mid-American",
        "Mountain West",
        "Pac-12",
        "SEC",
        "Sun Belt"
    ]);

    for (const game of games) {
        if (confs.has(game.home_conf) && game.attendance != null && game.week && game.week !== 15) {
            let week = weeks[game.week - 1];
            if (game.home_conf in week) {
                weeks[game.week - 1][game.home_conf]["attendance"] += game.attendance;
                weeks[game.week - 1][game.home_conf]["games"] += 1;
            } else {
                weeks[game.week - 1][game.home_conf] = {
                    "attendance": game.attendance,
                    "games": 1
                };
            }
        }
    }

    const data = weeks.map((week_data, week) => {
        let obj = {"week": week};
        for (const conf in week_data) {
            obj[conf] = Math.round(week_data[conf]["attendance"] / week_data[conf]["games"]);
        }
        return obj;
    });

    const colors = [
        "#FFE502",
        "#620E00",
        "#008F9C",
        "#98FF52",
        "#7544B1",
        "#B500FF",
        "#00FF78",
        "#FF6E41",
        "#005F39",
        "#6B6882",
        "#5FAD4E",
        "#A75740",
        "#A5FFD2",
        "#FFB167"
    ];
    return (
        <div>
        <h1 style={{
            textAlign: "center",
            fontWeight: "bold",
            fontSize: "34px"
        }}>Average Game Attendance by Conference</h1>
        <LineChart width={850} height={500} data={data}
            margin={{ top: 5, right: 30, left: 20, bottom: 5 }}>
            <CartesianGrid strokeDasharray="3 3" />
            <XAxis dataKey="week" label={{value: "Week"}}/>
            <YAxis label={{value: "Attendance", angle: -90, position: "left"}}/>
            <Tooltip />
            <Legend />
            {Array.from(confs).map((conf, idx) => (
                <Line key={conf} type="monotone" dataKey={conf} stroke={colors[idx]} strokeWidth={2.5}/>
            ))}
        </LineChart>
        </div>
    );
};

const VisPage = () => {
    const baseEndpoint = "https://api.ncaaccess.me";
    const [players, setTeam] = useState([]);
    const [games, setGame] = useState([]);

    useEffect(() => {
        const fetchPlayers = async () => {
            const res = await axios.get(baseEndpoint + "/api/players");
            setTeam(res.data);
        };
        const fetchGames = async () => {
            const res = await axios.get(baseEndpoint + "/api/games");
            setGame(res.data);
        };

        fetchPlayers();
        fetchGames();
    }, []);

    return (
        <div style={{backgroundColor: "#edf0f5"}}>
            <PlayerStates players={players}/>
            <PlayerSizes players={players}/>
            <AttendanceByWeek games={games}/>
        </div>
    );
};

export default VisPage;