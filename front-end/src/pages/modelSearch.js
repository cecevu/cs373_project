import React, { useState } from "react";
import { InputGroup, Form, Button } from "react-bootstrap";
import {
  StringParam,
  useQueryParams,
  withDefault,
  NumberParam,
} from "use-query-params";

const ModelSearch = () => {
  const [input, setInput] = useState("");
  const [params, setParams] = useQueryParams({
    page: withDefault(NumberParam, 1),
    search: withDefault(StringParam, ""),
  });

  return (
    <>
      <div
        className={`search`}
        style={{
          alignItems: "center",
          justifyContent: "center",
        }}
      >
        <center>
          <InputGroup
            className="mb-3 w-50"
            style={{
              alignItems: "center",
              justifyContent: "center",
            }}
          >
            <Form.Control
              className="w-50"
              placeholder="Search..."
              onChange={(e) => setInput(e.target.value)}
              value={input}
            />
            <Button
              variant="outline-primary"
              id="button-addon2"
              onClick={() => {
                console.log(input);
                setParams({ page: 1, search: input });
                window.location.href = window.location.href;
              }}
            >
              Enter
            </Button>
          </InputGroup>
        </center>
      </div>
    </>
  );
};

export default ModelSearch;
