import React, { useEffect, useState } from "react";
import {
  BarChart,
  Bar,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  Legend,
  PieChart,
  Pie,
  Radar,
  RadarChart,
  PolarGrid,
  PolarAngleAxis,
  PolarRadiusAxis,
} from "recharts";
import axios from "axios";

const PVisPage = () => {
  const [treatments, setTreatment] = useState([]);
  const [symptomCrit, setSymptomCrit] = useState([]);
  const [symptomSig, setSymptomSig] = useState([]);
  const [symptomLow, setSymptomLow] = useState([]);
  const [illnessCrit, setIllnessCrit] = useState([]);
  const [illnessSig, setIllnessSig] = useState([]);
  const [illnessLow, setIllnessLow] = useState([]);
  var administerCount = {
    INTRADERMAL: 0,
    TOPICAL: 0,
    "RESPIRATORY (INHALATION)": 0,
    ORAL: 0,
    INTRAVENOUS: 0,
    CUTANEOUS: 0,
    VAGINAL: 0,
    NASAL: 0,
    INTRAMUSCULAR: 0,
    PERCUTANEOUS: 0,
    SUBLINGUAL: 0,
    PARENTERAL: 0,
  };

  useEffect(() => {
    const fetchPosts = async () => {
      var res = await axios.get(
        "https://api.healthchecks.me/fda_data/?page_size=1000"
      );
      setTreatment(res.data.data);
      res = await axios.get(
        "https://api.healthchecks.me/symptoms/?urgent=low&sort=name"
      );
      setSymptomLow(res.data.data);
      res = await axios.get(
        "https://api.healthchecks.me/symptoms/?urgent=significant&sort=name"
      );
      setSymptomSig(res.data.data);
      res = await axios.get(
        "https://api.healthchecks.me/symptoms/?urgent=critical&sort=name"
      );
      setSymptomCrit(res.data.data);
      res = await axios.get(
        "https://api.healthchecks.me/illnesses/?urgent=low&sort=name"
      );
      setIllnessLow(res.data.data);
      res = await axios.get(
        "https://api.healthchecks.me/illnesses/?urgent=significant&sort=name"
      );
      setIllnessSig(res.data.data);
      res = await axios.get(
        "https://api.healthchecks.me/illnesses/?urgent=critical&sort=name"
      );
      setIllnessCrit(res.data.data);
    };

    fetchPosts();
  }, []);

  for (let i = 0; i < treatments.length; i++) {
    let treatment = treatments[i];
    if (treatment.route) {
      administerCount[treatment.route] = administerCount[treatment.route] + 1;
    }
  }

  var symptomCount = {
    Critical: symptomCrit.length,
    Significant: symptomSig.length,
    Low: symptomLow.length,
  };

  var illnessCount = {
    Critical: illnessCrit.length,
    Significant: illnessSig.length,
    Low: illnessLow.length,
  };

  const data = [];

  for (var key in administerCount) {
    var value = administerCount[key];
    var treeDict = { name: key, size: value };
    data.push(treeDict);
  }

  const data2 = [];

  for (key in symptomCount) {
    var value2 = symptomCount[key];
    var treeDict2 = { name: key, size: value2 };
    data2.push(treeDict2);
  }

  const data3 = [];

  for (key in illnessCount) {
    var value3 = illnessCount[key];
    var treeDict3 = { name: key, size: value3 };
    data3.push(treeDict3);
  }

  return (
    <div style={{backgroundColor: "#edf0f5"}}>
      <h1 style={{
            textAlign: "center",
            fontWeight: "bold",
            fontSize: "34px"
        }}>How treatments are administered</h1>
      <BarChart
        width={1800}
        height={800}
        data={data}
        margin={{
          top: 5,
          right: 10,
          left: 10,
          bottom: 5,
        }}
      >
        <CartesianGrid strokeDasharray="3 3" />
        <XAxis dataKey="name" />
        <YAxis />
        <Tooltip content={data} />
        <Legend />
        <Bar dataKey="size" barSize={20} fill="#8884d8" />
      </BarChart>
      <h style={{
            fontWeight: "bold",
            fontSize: "34px"
        }}>Urgency of Symptoms</h>
      <PieChart width={400} height={400}>
        <Pie
          dataKey="size"
          isAnimationActive={false}
          data={data2}
          cx="50%"
          cy="50%"
          outerRadius={80}
          fill="#34bdeb"
          label
        />
        <Tooltip />
      </PieChart>
      <h style={{
            fontWeight: "bold",
            fontSize: "34px"
        }}>Urgency of Treatments</h>
      <RadarChart height={500} width={500} outerRadius="80%" data={data3}>
        <PolarGrid />
        <PolarAngleAxis dataKey="name" />
        <PolarRadiusAxis />
        <Radar
          name="Mike"
          dataKey="size"
          stroke="#8884d8"
          fill="#8884d8"
          fillOpacity={0.6}
        />
      </RadarChart>
    </div>
  );
};

export default PVisPage;
