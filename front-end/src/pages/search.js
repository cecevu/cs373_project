import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import axios from "axios";
import Paginate from "./pagination";
import {
  StringParam,
  NumberParam,
  useQueryParams,
  withDefault,
} from "use-query-params";
import {
  Grid,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Paper,
} from "@mui/material";
import Highlighter from "react-highlight-words";
import Moment from "moment";
import Filter from "./filter";
import Sort from "./sort";
import ModelSearch from "./modelSearch";

const Search = () => {
  // const baseEndpoint = "http://localhost:5000";
  const baseEndpoint = "https://api.ncaaccess.me";
  const [team, setTeam] = useState([]);
  const [player, setPlayer] = useState([]);
  const [game, setGame] = useState([]);
  const [loading, setLoading] = useState(false);

  const [params, setParams] = useQueryParams({
    page: withDefault(NumberParam, 1),
    search: withDefault(StringParam, ""),
  });

  useEffect(() => {
    const fetchPosts = async () => {
      setLoading(true);
      let query = "";
      query += "&search=" + params.search;
      console.log(query);
      if (params.search) {
        const t = await axios.get(baseEndpoint + "/api/teams?" + query);
        setTeam(t.data);
        const p = await axios.get(baseEndpoint + "/api/players?" + query);
        setPlayer(p.data);
        const g = await axios.get(baseEndpoint + "/api/games?" + query);
        setGame(g.data);
        setLoading(false);
      }
    };

    fetchPosts();
  }, []);

  const DisplayTeam = () => {
    return (
      <div>
        <div className="text-center text-2xl font-sans">
          <b>Teams</b>
        </div>
        {/* Container */}
        <div className="grid sm:grid-cols-10 md:grid-cols-5 gap-2 m-3">
          {/* Grid Item */}
          {team.map((result) => (
            <Link
              to={`./${result.team_id}`}
              style={{
                backgroundImage: `url(${result.logos1})`,
                backgroundSize: "cover",
                display: "block",
                textDecoration: "none",
                minHeight: "150px",
                maxHeight: "550px",
                paddingBottom: "100%",
              }}
              className="shadow-lg shadow-[#160904] group container rounded-md flex justify-center items-center mx-auto content-div"
              key={result.team_id}
            >
              {/* Hover Effects */}
              <div
                style={{
                  textAlign: "center",
                  verticalAlign: "middle",
                }}
                className="opacity-0 group-hover:opacity-100 text-center py-10"
              >
                <b> School </b>
                <p>
                  {" "}
                  <Highlighter
                    autoEscape={true}
                    textToHighlight={result.school_name}
                    searchWords={params.search.split(/[%20 \s,;/]+/)}
                  />{" "}
                </p>
                <b> City </b>
                <p>
                  {" "}
                  <Highlighter
                    autoEscape={true}
                    textToHighlight={result.city}
                    searchWords={params.search.split(/[%20 \s,;/]+/)}
                  />{" "}
                </p>
                <b> State </b>
                <p>
                  {" "}
                  <Highlighter
                    autoEscape={true}
                    textToHighlight={result.state_name}
                    searchWords={params.search.split(/[%20 \s,;/]+/)}
                  />{" "}
                </p>
                <b> Conference </b>
                <p>
                  {" "}
                  <Highlighter
                    autoEscape={true}
                    textToHighlight={result.conference}
                    searchWords={params.search.split(/[%20 \s,;/]+/)}
                  />{" "}
                </p>
              </div>
            </Link>
          ))}
        </div>
      </div>
    );
  };

  const DisplayPlayer = () => {
    return (
      <div>
        <div className="text-center text-2xl font-sans">
          <b>Players</b>
        </div>
        <div>
          <Grid container justifyContent="center">
            <TableContainer component={Paper} style={{ width: "80%" }}>
              <Table component="div" sx={{ minWidth: 650 }}>
                <TableHead component="div">
                  <TableRow component="div">
                    <TableCell component="div" align="center">
                      First Name
                    </TableCell>
                    <TableCell component="div" align="center">
                      Last Name
                    </TableCell>
                    <TableCell component="div" align="center">
                      Team
                    </TableCell>
                    <TableCell component="div" align="center">
                      Position
                    </TableCell>
                    <TableCell component="div" align="center">
                      Conference
                    </TableCell>
                  </TableRow>
                </TableHead>
                <TableBody component="div">
                  {player.map((result) => (
                    <TableRow
                      hover
                      key={result.player_id}
                      component={Link}
                      to={`./${result.player_id}`}
                      style={{ textDecoration: "none" }}
                    >
                      <TableCell component="div" align="center">
                        <Highlighter
                          autoEscape={true}
                          textToHighlight={result.first_name}
                          searchWords={params.search.split(/[%20 \s,;/]+/)}
                        />
                      </TableCell>
                      <TableCell component="div" align="center">
                        <Highlighter
                          autoEscape={true}
                          textToHighlight={result.last_name}
                          searchWords={params.search.split(/[%20 \s,;/]+/)}
                        />
                      </TableCell>
                      <TableCell component="div" align="center">
                        <Highlighter
                          autoEscape={true}
                          textToHighlight={result.team}
                          searchWords={params.search.split(/[%20 \s,;/]+/)}
                        />
                      </TableCell>
                      <TableCell component="div" align="center">
                        <Highlighter
                          autoEscape={true}
                          textToHighlight={result.position}
                          searchWords={params.search.split(/[%20 \s,;/]+/)}
                        />
                      </TableCell>
                      <TableCell component="div" align="center">
                        <Highlighter
                          autoEscape={true}
                          textToHighlight={result.conf}
                          searchWords={params.search.split(/[%20 \s,;/]+/)}
                        />
                      </TableCell>
                    </TableRow>
                  ))}
                </TableBody>
              </Table>
            </TableContainer>
          </Grid>
        </div>
      </div>
    );
  };

  const DisplayGames = () => {
    return (
      <div>
        <div className="text-center text-2xl font-sans">
          <b>Games</b>
        </div>
        <Grid container justifyContent="center">
          <TableContainer component={Paper} style={{ width: "80%" }}>
            <Table component="div" sx={{ minWidth: 650 }}>
              <TableHead component="div">
                <TableRow component="div">
                  <TableCell component="div" align="center">
                    Home Team
                  </TableCell>
                  <TableCell component="div" align="center">
                    Away Team
                  </TableCell>
                  <TableCell component="div" align="center">
                    Date
                  </TableCell>
                  <TableCell component="div" align="center">
                    Winner
                  </TableCell>
                  <TableCell component="div" align="center">
                    season
                  </TableCell>
                </TableRow>
              </TableHead>
              <TableBody component="div">
                {game.map((result) => (
                  <TableRow
                    hover
                    key={result.game_id}
                    component={Link}
                    to={`./${result.game_id}`}
                    style={{ textDecoration: "none" }}
                  >
                    <TableCell component="div" scope="center">
                      <Highlighter
                        autoEscape={true}
                        textToHighlight={result.home_team}
                        searchWords={params.search.split(/[%20 \s,;/]+/)}
                      />
                    </TableCell>
                    <TableCell component="div" align="center">
                      <Highlighter
                        autoEscape={true}
                        textToHighlight={result.away_team}
                        searchWords={params.search.split(/[%20 \s,;/]+/)}
                      />
                    </TableCell>
                    <TableCell component="div" align="center">
                      {Moment(result.start_date).format("MMM d, y")}
                    </TableCell>
                    <TableCell component="div" align="center">
                      {result.home_points > result.away_points
                        ? result.home_team
                        : result.away_team}
                    </TableCell>
                    <TableCell component="div" align="center">
                      {result.season}
                    </TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </TableContainer>
        </Grid>
      </div>
    );
  };

  return (
    <div className="container mt-5" style={{
      backgroundColor: "#edf0f5df"
    }}>
      <ModelSearch></ModelSearch>
      {loading ? (
        loading
      ) : (
        <>
          <DisplayTeam />
          <DisplayGames />
          <DisplayPlayer />
        </>
      )}
    </div>
  );
};

export default Search;
