import Button from "react-bootstrap/Button";
import Card from "react-bootstrap/Card";
import { Link } from "react-router-dom";
import bg from "../pictures/background.png";
import stadium from "../pictures/stadium.jpg";
import Image from "react-bootstrap/Image";

// export default HorizontalExample;
function HomeCard() {
  return (
    <div
      style={{
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        height: "80vh",
      }}
    >
      <div class="row">
        <div>
          <Card style={{ width: "18rem", backgroundColor: "#edf0f5df" }}>
            <Card.Img
              variant="top"
              src="https://cdn.shopify.com/s/files/1/0480/9470/7866/collections/8b1cbe27fec5c973222e6aaf0cd87522.png?v=1647462719"
            />
            <Card.Body>
              <Card.Title>Teams</Card.Title>
              <Card.Text>Contains Information about each Team</Card.Text>
              <Link to="/teams" activeStyle>
                <Button id="teams-btn" variant="outline-primary">
                  Go to Teams
                </Button>
              </Link>
            </Card.Body>
          </Card>
        </div>
        <div>
          <Card style={{ width: "18rem", backgroundColor: "#edf0f5df" }}>
            <Card.Img
              width="100"
              height="100"
              variant="top"
              src="https://a4.espncdn.com/combiner/i?img=/photo/2022/0910/r1059940_1296x729_16-9.jpg"
            />
            <Card.Body>
              <Card.Title>Players</Card.Title>
              <Card.Text>Contains Information about each Player</Card.Text>
              <Link to="/players" activeStyle>
                <Button id="players-btn" variant="outline-primary">
                  Go to Players
                </Button>
              </Link>
            </Card.Body>
          </Card>
        </div>
        <div>
          <Card style={{ width: "18rem", backgroundColor: "#edf0f5df" }}>
            <Card.Img
              variant="top"
              src="https://a.espncdn.com/photo/2010/0815/ncf_game03_800.jpg"
            />
            <Card.Body>
              <Card.Title>Games</Card.Title>
              <Card.Text>Contains Information about each Game</Card.Text>
              <Link to="/games" activeStyle>
                <Button id="games-btn" variant="outline-primary">
                  Go to Games
                </Button>
              </Link>
            </Card.Body>
          </Card>
        </div>
      </div>
    </div>
  );
}

function Home() {
  return (
    <div
      style={{
        backgroundSize: "cover",
        backgroundRepeat: "no-repeat",
        backgroundImage: `url(${stadium})`
      }}
    >
      <center>
        <Image
          src={bg}
          rounded
          style={{ width: "480px", height: "320px", paddingTop: "20px" }}
        />
        <h1 style={{ fontSize: "70px" }}>
          <big>
            <strong> Welcome To NCAAccess</strong>
          </big>
        </h1>
      </center>
      <HomeCard />
    </div>
  );
}
export default Home;
