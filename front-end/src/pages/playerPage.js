import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import axios from "axios";
import Paginate from "./pagination";
import {
  Grid,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Paper,
} from "@mui/material";
import {
  StringParam,
  NumberParam,
  useQueryParams,
  withDefault,
} from "use-query-params";
import Highlighter from "react-highlight-words";
import Filter from "./filter";
import Sort from "./sort";
import ModelSearch from "./modelSearch";

const PlayerPage = () => {
  const [team, setTeam] = useState([]);
  const [loading, setLoading] = useState(false);
  const [postsPerPage] = useState(20);
  // const baseEndpoint = "http://localhost:5000";
  const baseEndpoint = "https://api.ncaaccess.me";

  const [params, setParams] = useQueryParams({
    page: withDefault(NumberParam, 1),
    search: withDefault(StringParam, ""),
    sort: withDefault(StringParam, ""),
    conf: withDefault(StringParam, ""),
    team: withDefault(StringParam, ""),
    position: withDefault(StringParam, ""),
  });

  useEffect(() => {
    const fetchPosts = async () => {
      setLoading(true);
      let query = ""
      if (params.conf)
        query += "&conf=" + params.conf
      if (params.team)
        query += "&team=" + params.team
      if (params.position)
        query += "&position=" + params.position
      if (params.sort)
        query += "&sort=" + params.sort
      if (params.search)
        query += "&search=" + params.search 
      console.log(query)

      const res = await axios.get(baseEndpoint + "/api/players?" + query);
      setTeam(res.data);
      setLoading(false);
    };

    fetchPosts();
  }, []);

  // Get current posts
  const indexOfLastPost = params.page * postsPerPage;
  const indexOfFirstPost = indexOfLastPost - postsPerPage;
  const currentPosts = team.slice(indexOfFirstPost, indexOfLastPost);

  // Change page
  const paginate = (pageNumber) => setParams({...params, page: pageNumber});

  const Posts = ({ posts, loading }) => {
    if (loading) {
      return <h2>Loading...</h2>;
    }

    return (
      <div>
        <div className="text-center text-2xl font-sans text-slate-500">
          <b>Players</b>
        </div>
        <div>
          <Grid container justifyContent="center">
            <TableContainer component={Paper} style={{ width: "80%" }}>
              <Table component="div" sx={{ minWidth: 650 }}>
                <TableHead component="div">
                  <TableRow component="div">
                    <TableCell component="div" align="center">
                      First Name
                    </TableCell>
                    <TableCell component="div" align="center">
                      Last Name
                    </TableCell>
                    <TableCell component="div" align="center">
                      Team
                    </TableCell>
                    <TableCell component="div" align="center">
                      Position
                    </TableCell>
                    <TableCell component="div" align="center">
                      Conference
                    </TableCell>
                  </TableRow>
                </TableHead>
                <TableBody component="div">
                  {posts.map((result) => (
                    <TableRow
                      hover
                      key={result.player_id}
                      component={Link}
                      to={`./${result.player_id}`}
                      style={{ textDecoration: "none" }}
                    >
                      <TableCell component="div" align="center">
                      <Highlighter autoEscape={true} textToHighlight={result.first_name} searchWords={params.search.split(/[%20 \s,;/]+/)}/>
                      </TableCell>
                      <TableCell component="div" align="center">
                      <Highlighter autoEscape={true} textToHighlight={result.last_name} searchWords={params.search.split(/[%20 \s,;/]+/)}/>
                      </TableCell>
                      <TableCell component="div" align="center">
                      <Highlighter autoEscape={true} textToHighlight={result.team} searchWords={params.search.split(/[%20 \s,;/]+/)}/>
                      </TableCell>
                      <TableCell component="div" align="center">
                      <Highlighter autoEscape={true} textToHighlight={result.position} searchWords={params.search.split(/[%20 \s,;/]+/)}/>
                      </TableCell>
                      <TableCell component="div" align="center">
                      <Highlighter autoEscape={true} textToHighlight={result.conf} searchWords={params.search.split(/[%20 \s,;/]+/)}/>
                      </TableCell>
                    </TableRow>
                  ))}
                </TableBody>
              </Table>
            </TableContainer>
          </Grid>
        </div>
      </div>
    );
  };

  return (
    <div className="container mt-5" style={{backgroundColor: "#edf0f5"}}>
      <ModelSearch></ModelSearch>
      <Filter
        filter={["Team", "Conf", "Position"]}
        filterOptions={[["Air Force", "Akron", "Alabama", "Appalachian State", "Arizona", "Arizona State",
         "Arkansas", "Arkansas State", "Army", "Auburn", "Ball State", "Baylor", "Boise State", "Boston College",
         "Bowling Green", "Buffalo", "BYU", "California", "Central Michigan", "Charlotte", "Cincinnati", "Clemson",
        "Coastal Carolina", "Colorado", "Colorado State", "Duke", "East Carolina", "Eastern Michigan", "Florida Atlantic",
        "FIU", "Florida", "Florida State", "Fresno State", "Georgia", "Georgia Southern", "Georgia State", "Georgia Tech",
        "Hawaii", "Houston", "Illinois", "Indiana", "Iowa", "Iowa State", "James Madison", "Kansas", "Kansas State",
        "Kent State", "Kentucky", "Liberty", "Louisiana", "Louisiana–Monroe", "Louisiana Tech", "Louisville", "LSU",
        "Marshall", "Maryland", "Memphis", "Miami", "Miami", "Michigan", "Michigan State", "Middle Tennessee",
        "Minnesota", "Mississippi State", "Missouri", "Navy", "NC State", "Nebraska", "Nevada", "New Mexico",
        "New Mexico State", "North Carolina", "North Texas", "Northern Illinois", "Northwestern", "Notre Dame",
        "Ohio", "Ohio State", "Oklahoma", "Oklahoma State", "Old Dominion", "Ole Miss", "Oregon", "Oregon State",
        "Penn State", "Pittsburgh", "Purdue", "Rice", "Rutgers", "San Diego State", "San Jose State", "SMU",
        "South Alabama", "South Carolina", "South Florida", "Southern Miss", "Stanford", "Syracuse", "TCU", "Temple",
        "Tennessee", "Texas", "Texas A&M", "Texas State", "Texas Tech", "Toledo", "Troy", "Tulane", "Tulsa", "UAB", "UCF",
        "UCLA", "UConn", "UMass", "UNLV", "USC", "UTEP", "UTSA", "Utah", "Utah State", "Vanderbilt", "Virginia",
        "Virginia Tech", "Wake Forest", "Washington", "Washington State", "West Virginia", "Western Kentucky",
        "Western Michigan", "Wisconsin", "Wyoming"], 
          ["Big 12","Mid-American", "Mountain West", "Pac-12", "American Athletic", "Sun Belt", "Big Ten",
          "FBS Independents", "Conference USA", "SEC", "ACC"], 
        ["QB", "RB", "FB", "WR", "TE"]]}
      />
      <Sort SortOptions={["First Name", "Last Name", "Team"]} SortNames={["first_name", "last_name", "team"]} />
      <Posts posts={currentPosts} loading={loading} />
      <center>
        <Paginate
          postsPerPage={postsPerPage}
          totalPosts={team.length}
          paginate={paginate}
          currentPage={params.page}
        />
        <div>Total Results = {team.length}</div>
      </center>
    </div>
  );
};

export default PlayerPage;
