import React from "react";
import { useState, useEffect } from "react";
import axios from "axios";
import dk from "../pictures/dk.png";
import gc from "../pictures/gus_caldwell.png";
import cv from "../pictures/cece.png";
import wb from "../pictures/will.png";
import bf from "../pictures/bruno.png";

const About = () => {
  const [data, setData] = useState([]);
  const [numIssues, setNumIssues] = useState(null);
  const fetchData = () => {
    const pages = [];
    var page = 1;
    while (page < 10) {
      const commitsAPI =
        "https://gitlab.com/api/v4/projects/39736412/repository/commits?per_page=100&page=" +
        page;
      const getCommits = axios.get(commitsAPI);
      pages.push(getCommits);
      page++;
    }
    axios.all(pages).then(
      axios.spread((...allPages) => {
        let updateData = [];
        for (let i = 0; i < allPages.length; i++) {
          for (let j = 0; j < allPages[i].data.length; j++) {
            updateData.push(allPages[i].data[j]);
          }
        }
        setData(updateData);
      })
    );
  };

  const fetchNumIssues = () => {
    const issuesAPI =
      "https://gitlab.com/api/v4/projects/39736412/issues?state=closed&per_page=100";
    const getIssues = axios.get(issuesAPI);
    axios.all([getIssues]).then(
      axios.spread((...numIssues) => {
        setNumIssues(numIssues[0].data);
      })
    );
  };

  useEffect(() => {
    fetchData();
    fetchNumIssues();
  }, []);

  return (
    <div style={{
      backgroundColor: "#edf0f5"
    }}>
      <div name="about" className="w-full h-screen text-gray-300">
        <div className="flex flex-col justify-center items-center w-full ">
          <div className="max-w-[1000px] w-full grid grid-cols-2 gap-8">
            <div className="sm:text-right text-black pb-8 pl-4 my-4">
              <p className="text-4xl font-bold inline border-b-4 border-orange-600">
                About
              </p>
            </div>
            <div></div>
          </div>
          <div className="max-w-[1000px] text-black w-full grid sm:grid-cols-2 gap-8 px-4">
            <div className="sm:text-right text-4xl font-bold">
              <p>Welcome to NCAAccess</p>
            </div>
            <div>
              <p>
                This site was designed as a hub for college football enthusiasts
                to stay updated on the latest developments in the D1 field. We
                provide live info on the state of various NCAA-affiliated
                football teams, players, and games.
              </p>
              <p>
                {" "}
                <br />{" "}
              </p>
              <p>
                Upon integration of our various data points, we found a
                correlation between the city a team belonged to and their win
                rate. Essentially, more popular cities yielded a higher win
                rate.{" "}
              </p>
            </div>
          </div>
        </div>
      </div>

      <div
        name="team"
        className="w-full md:h-screen"
      >
        <div className="max-w-[1000px] mx-auto p-4 flex flex-col justify-center w-full h-full">
          <div className="pb-8">
            <p className="text-4xl text-black font-bold inline border-b-4 text-gray-300 border-orange-600">
              GitLab Stats + Group
            </p>
            <p className="py-6">
              Here's the statistics fetched from the GitLab API along with our
              SWE group
            </p>
            <p>
              {" "}
              <br />{" "}
            </p>
            <p>
              Total Commits: {data.length > 0 && data.length} | Total Issues:{" "}
              {numIssues != null && numIssues.length} | Unit Tests: 0
            </p>
            <p>
              {" "}
              <br />{" "}
            </p>
            <p>
              {" "}
              <br />{" "}
            </p>
            <p>
              All API's were scraped using Postman and Python Script. The links
              are as follows:
            </p>
            <p>
              {" "}
              <br />{" "}
            </p>
            <a href="https://documenter.getpostman.com/view/23646636/2s83tCMDg2" style={{color: "#007bff"}}>
              Postman Documentation Link
            </a>
            <p>
              {" "}
              <br />{" "}
            </p>

            <a href="https://api.collegefootballdata.com/api/docs/?url=/api-docs.json" style={{color: "#007bff"}}>
              NCAA Data
            </a><br/>
            <a href="https://www.microsoft.com/en-us/bing/apis/bing-image-search-api" style={{color: "#007bff"}}>
              Bing Image Search
            </a><br/>
            <a href="https://www.microsoft.com/en-us/bing/apis/bing-video-search-api" style={{color: "#007bff"}}>
              Bing Video Search
            </a><br/>
            <p>
              {" "}
              <br />{" "}
            </p>
            <p>
              <p>
                {" "}
                <br />{" "}
              </p>
              Tools Used:
            </p>
            <p>
              <ol>
                <li>
                  Discord - Platform for communication with team where we
                  scheduled meetings and sent resources to the group
                </li>
                <li>
                  Zoom - Platform for video calls where we held scrum meetings
                  and worked on the project live
                </li>
                <li>
                  VSCode - IDE where we write our code, install packages, and
                  push to repo
                </li>
                <li>
                  React - Library for JS that allows us to import and utilize
                  components for our web app
                </li>
                <li>
                  Tailwind CSS - Framework for writing CSS within JS file as
                  opposed to making separate CSS file
                </li>
                <li>
                  Postman - Platform for designing and building our API and its
                  documentation to scrape data from our sources
                </li>
                <li>
                  AWS Amplify - Platform for hosting the domain of our website{" "}
                </li>
                <li>
                  Bootstrap - React-Bootstrap is a framework for frontend
                  development that allows the import of various components to
                  utilize in our website
                </li>
                <li>
                  GitLab - Repository to store our code and allows version
                  control through the CLI
                </li>
                <li>NameCheap - Used to register domain for our website</li>
              </ol>
            </p>
            <p>
              {" "}
              <br />{" "}
            </p>
          </div>

          {/* Container */}
          <div className="grid sm:grid-cols-2 md:grid-cols-3 gap-4">
            {/* Grid Item */}
            <div
              style={{ backgroundImage: `url(${dk})` }}
              className="shadow-lg shadow-[#160904] group container rounded-md flex justify-center items-center mx-auto content-div"
            >
              {/* Hover Effects */}
              <div className="opacity-0 group-hover:opacity-100 text-center">
                <span className="text-lg font-bold text-white tracking-wider">
                  Dheeraj Kashyap (Frontend)
                </span>
                <div className="pt-8 text-center">
                  I'm a junior CS major at UT Austin with an interest in
                  blockchain tech and ML. In my free time, I enjoy
                  weightlifting, exploring new places, and playing games.
                </div>
                <p className="text-sm">
                  DK Commits:{" "}
                  {data.length > 0 &&
                    data.filter(
                      (x) =>
                        x["committer_name"] === "dheerajkash07" ||
                        x["committer_name"] === "Dheeraj Kashyap"
                    ).length}{" "}
                  | DK Issues:{" "}
                  {numIssues != null &&
                    numIssues.filter(
                      (x) =>
                        x["closed_by"]["username"] === "dheerajkash07" ||
                        x["closed_by"]["name"] === "Dheeraj Kashyap"
                    ).length}
                </p>
              </div>
            </div>

            <div
              style={{ backgroundImage: `url(${cv})` }}
              className="shadow-lg shadow-[#040c16] group container rounded-md flex justify-center items-center mx-auto content-div"
            >
              {/* Hover Effects */}
              <div className="opacity-0 group-hover:opacity-100 text-center">
                <span className="text-lg font-bold text-white tracking-wider">
                  Cece Vu (Frontend)
                </span>
                <div className="pt-8 text-center">
                  I'm a Senior Computer Science Major at UT Austin with an
                  interest in ML and data science. In my free time, I like
                  baking and playing games with my friends
                </div>
                <p className="text-sm">
                  CV Commits:{" "}
                  {data.length > 0 &&
                    data.filter(
                      (x) =>
                        x["committer_name"] === "cecevu" ||
                        x["committer_name"] === "Cecilia Q Vu"
                    ).length}{" "}
                  | CV Issues:{" "}
                  {numIssues != null &&
                    numIssues.filter(
                      (x) =>
                        x["closed_by"]["username"] === "cecevu" ||
                        x["closed_by"]["name"] === "Cecilia Q Vu"
                    ).length}
                </p>
              </div>
            </div>

            {/* Grid Item */}
            <div
              style={{ backgroundImage: `url(${wb})` }}
              className="shadow-lg shadow-[#040c16] group container rounded-md flex justify-center items-center mx-auto content-div"
            >
              {/* Hover Effects */}
              <div className="opacity-0 group-hover:opacity-100 text-center">
                <span className="text-lg font-bold text-white tracking-wider">
                  Willard Bui (Frontend)
                </span>
                <div className="pt-8 text-center">
                  I'm a Junior CS Major at UT Austin with an interest ML and AI.
                  I love playing LOL and WOW. In my freetime, I also like to mod
                  subreddits.
                </div>
                <p className="text-sm">
                  WB Commits:{" "}
                  {data.length > 0 &&
                    data.filter(
                      (x) =>
                        x["committer_name"] === "willardbui" ||
                        x["committer_name"] === "Willard Bui"
                    ).length}{" "}
                  | WB Issues:{" "}
                  {numIssues != null &&
                    numIssues.filter(
                      (x) =>
                        x["closed_by"]["username"] === "willardbui" ||
                        x["closed_by"]["name"] === "will bui"
                    ).length}
                </p>
              </div>
            </div>

            <div
              style={{
                backgroundImage: `url(${gc})`,
                backgroundSize: "contain",
              }}
              className="shadow-lg shadow-[#040c16] group container rounded-md flex justify-center items-center mx-auto content-div"
            >
              {/* Hover Effects */}
              <div className="opacity-0 group-hover:opacity-100 text-center">
                <span className="text-lg font-bold text-white tracking-wider">
                  Gus Caldwell (Fullstack)
                </span>
                <div className="pt-8 text-center">
                  I'm a 5th year CS major at UT Austin interested in machine
                  learning and game development. Outside of class I like to disc
                  golf and hang out with my cat.
                </div>
                <p className="text-sm">
                  GC Commits:{" "}
                  {data.length > 0 &&
                    data.filter(
                      (x) =>
                        x["committer_name"] === "skinnertx" ||
                        x["committer_name"] === "Gus Caldwell"
                    ).length}{" "}
                  | GC Issues:{" "}
                  {numIssues != null &&
                    numIssues.filter(
                      (x) =>
                        x["closed_by"]["username"] === "skinnertx" ||
                        x["closed_by"]["name"] === "Gus Caldwell"
                    ).length}
                </p>
              </div>
            </div>

            <div
              style={{ backgroundImage: `url(${bf})` }}
              className="shadow-lg shadow-[#040c16] group container rounded-md flex justify-center items-center mx-auto content-div"
            >
              {/* Hover Effects */}
              <div className="opacity-0 group-hover:opacity-100 text-center">
                <span className="text-lg font-bold text-white tracking-wider">
                  Bruno Fazzani (Backend)
                </span>
                <div className="pt-8 text-center">
                  I'm a Senior CS major at UT Austin interested in machine
                  learning and computational game theory/playing. Outside of
                  tech, I enjoy rock climbing and exploring restaurants in
                  Austin.
                </div>
                <p className="text-sm">
                  BF Commits:{" "}
                  {data.length > 0 &&
                    data.filter(
                      (x) =>
                        x["committer_name"] === "bfazzani" ||
                        x["committer_name"] === "Bruno Fazzani"
                    ).length}{" "}
                  | BF Issues:{" "}
                  {numIssues != null &&
                    numIssues.filter(
                      (x) =>
                        x["closed_by"]["username"] === "bfazzani1" ||
                        x["closed_by"]["name"] === "Bruno Fazzani"
                    ).length}
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default About;
