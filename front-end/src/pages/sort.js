import React from "react";
import { Dropdown, DropdownButton, ButtonGroup } from "react-bootstrap";
import { StringParam, useQueryParam, withDefault } from "use-query-params";

const Sort = ({SortOptions, SortNames }) => {

  const [params, setParams] = useQueryParam("sort", withDefault(StringParam, ""));

  return (
    <>
      <div
        className={`sort`}
        style={{
          textAlign: "center",
          verticalAlign: "middle",
          color: "black",
        }}
      >
            <DropdownButton
              key="0"
              className="filter_item"
              as={ButtonGroup}
              variant="outline-primary" 
              title={`Sort by ...`}
              style={{
                textAlign: "center",
                verticalAlign: "middle",
                color: "black",
              }}
            >
              {SortOptions.map((i, j) => {
                let opt = i.toString();
                opt = SortNames[j]
                console.log(opt)
                return (
                  <Dropdown.Item
                    key={j}
                    onClick={() => {setParams(opt); window.location.href = window.location.href}}>
                    {`${i}`}
                  </Dropdown.Item>
                );
              })}
              <Dropdown.Item
                    key={SortOptions.length + 1}
                    onClick={() => {setParams(""); window.location.href = window.location.href}}
                  >
                    No Sort
                  </Dropdown.Item>
            </DropdownButton>
      </div>
    </>
  );
};

export default Sort;