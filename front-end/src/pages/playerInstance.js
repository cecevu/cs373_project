import React from 'react';
import Image from "react-bootstrap/Image";
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Accordion from 'react-bootstrap/Accordion'
import Card from 'react-bootstrap/Card';
import stadium from '../pictures/stadium.jpg';

import { Link, useParams } from 'react-router-dom'
import { useState, useEffect } from 'react';
import axios from "axios";

const PlayerInstance = () => {
    const { id } = useParams();
    const [player, setPlayer] = useState([]);
    const [team, setTeam] = useState([]);
    const [games, setGame] = useState([]);
    const [loading, setLoading] = useState(true);

    const endpoint = "https://api.ncaaccess.me/api/players/"

    useEffect(() => {
        setLoading(true);
        axios
            .get(endpoint + id)
            .then((res) => {
                console.log(res);
                setPlayer(res.data);
            })
        axios
            .get("https://api.ncaaccess.me/api/teams")
            .then((res) => {
                console.log(res);
                setTeam(res.data);
            })
            .catch((e) => console.log(e));
        axios
            .get("https://api.ncaaccess.me/api/games")
            .then((res) => {
                console.log(res);
                setGame(res.data);
            })
            .catch((e) => console.log(e));
        setLoading(false)
    }, [endpoint, id]);

    let player_team = {};
    for (let i = 0; i < team.length; i++) {
        if (team[i].school_name === player.team) {
            player_team = team[i]
        }
    }
    console.log(player_team)
    let player_games = []

    for (let i = 0; i < games.length; i++) {
        if (player.season === games[i].season) {
            if (player.team === games[i].away_team || player.team === games[i].home_team) {
                player_games.push(games[i])
            }
        }
    }
    player_games.sort(function (a, b) {
        return a.week - b.week;
    });

    const ShowPlayer = () => {
        return (
            <Container style={{backgroundSize: "cover", backgroundImage: `url(${stadium})`}}>
                <Row style={{ padding: '5px' }}>
                    <h1 style={{ fontSize: '40px' }}><big><strong>{player.first_name + " " + player.last_name} from <Link to={`../teams/${player_team.team_id}`}
                        style={{ color: player_team.color }}>{player.team}</Link></strong></big></h1>
                    <Image src={player_team.logos1} rounded style={{ width: '50px', height: '60px', paddingLeft: "10px" }} />
                </Row>
                <Row>
                    <Col>
                        <Image src={player.photo1} rounded style={{ width: "650px" }} />
                    </Col>
                    <Col style={{ textAlign: 'center', fontSize: '25px' }}>
                        <h2><b>Jersey Number</b></h2>
                        <p>{player.jersey}</p>
                        <h2><b>Height</b></h2>
                        <p>{player.height} inches</p>
                        <h2><b>Weight</b></h2>
                        <p>{player.weight} pounds</p>
                    </Col>
                    <Col style={{ textAlign: 'center', fontSize: '18px' }}>
                        <Row style={{ fontSize: '30px' }}><h2 className='m-auto'><b>Stats</b></h2></Row>
                        <h2><b>Overall: </b>{player.overall}</h2>
                        <h2><b>Passing: </b>{player.pass}</h2>
                        <h2><b>Rushing: </b>{player.rush}</h2>
                        <h2><b>First Down: </b>{player.first_down}</h2>
                        <h2><b>Second Down: </b>{player.second_down}</h2>
                        <h2><b>Third Down: </b>{player.third_down}</h2>
                        <h2><b>Standard Downs: </b>{player.standard_downs}</h2>
                        <h2><b>Passing Downs: </b>{player.passing_downs}</h2>
                    </Col>
                </Row>
                <Card style={{backgroundColor: "#edf0f5df"}}>
                    <Accordion defaultActiveKey="0" style={{ textAlign: 'center' }}>
                        <Accordion.Item eventKey="0">
                            <Card.Header>
                                <Accordion.Header>Player Game Schedule</Accordion.Header>
                            </Card.Header>
                            <Accordion.Body>
                                <table className="table">
                                    <thead>
                                        <tr>
                                            <th>Week</th>
                                            <th>Home Team</th>
                                            <th>Away Team</th>
                                            <th>Date</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {player_games.map((game) => {
                                            let link = "../games/" + game.game_id;
                                            return (
                                                <tr key={game.game_id}>
                                                    <td><Link to={link} style={{ color: 'blue' }}> {game.week}</Link></td>
                                                    <td> {game.home_team}</td>
                                                    <td> {game.away_team}</td>
                                                    <td> {game.start_date.substring(0, 10)}</td>
                                                </tr>
                                            );
                                        }

                                        )}
                                    </tbody>
                                </table>
                            </Accordion.Body>
                        </Accordion.Item>
                    </Accordion>
                </Card>


            </Container>
        );
    }

    if (loading) {
        return <div className="App">Loading...</div>;
    }

    return <>{<ShowPlayer />}</>;

};

export default PlayerInstance;
