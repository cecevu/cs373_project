import React, { useState, useEffect } from "react";
import { useParams } from "react-router-dom";
import axios from "axios";
import Card from "react-bootstrap/Card";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import { Link } from "react-router-dom";
import Moment from "moment";
import Accordion from "react-bootstrap/Accordion";
import Container from "react-bootstrap/Container";
import stadium from "../pictures/stadium.jpg";


const GameInstance = () => {
  const { id } = useParams();
  // const baseEndpoint = "http://localhost:5000";
  const baseEndpoint = "https://api.ncaaccess.me"
  const endpoint = baseEndpoint + "/api/games/";
  // const endpoint = 'http://localhost:5000/api/teams/1';
  const [team, setTeam] = useState([]);
  const [game, setGame] = useState([]);
  const [player, setPlayer] = useState([]);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    setLoading(true);
    axios
      .get(endpoint + id)
      .then((res) => {
        setGame(res.data);
      })
      .catch((e) => console.log(e));
    axios
      .get(baseEndpoint + "/api/teams")
      .then((res) => {
        setTeam(res.data);
      })
      .catch((e) => console.log(e));
    axios
      .get(baseEndpoint + "/api/players")
      .then((res) => {
        setPlayer(res.data);
      })
      .catch((e) => console.log(e));
    setLoading(false)
  }, [endpoint, id]);

  var awayTeam = {};
  var homeTeam = {};
  for (var i = 0; i < team.length; i++) {
    if (game.away_team === team[i].school_name) {
      awayTeam = team[i];
    } else if (game.home_team === team[i].school_name) {
      homeTeam = team[i];
    }
  }

  var homePlayer = [];
  var awayPlayer = [];

  for (i = 0; i < player.length; i++) {
    if (player[i].team === game.away_team && game.season === player[i].season) {
      awayPlayer.push(player[i]);
    } else if (player[i].team === game.home_team && game.season === player[i].season) {
      homePlayer.push(player[i]);
    }
  }

  const ShowGame = () => {
    return (
      <Container style={{backgroundSize: "cover", backgroundImage: `url(${stadium})`}}>
        <Row>
          <Col style={{
            margin: "90px",
            borderRadius: "5px",
            paddingLeft: "20px",
          }}>
            <Row>
              <b>
                {" "}
                {game.home_team} v {game.away_team}{" "}
              </b>
            </Row>
            <Row>
              <b> Home Team: </b>
              <p>
                <Link to={`../teams/${homeTeam.team_id}`}>{game.home_team}</Link>
              </p>
            </Row>
            <Row>
              <b> Away Team: </b>
              <p>
                <Link to={`../teams/${awayTeam.team_id}`}>{game.away_team}</Link>
              </p>
            </Row>
            <Row>
              <b>Score: </b>{" "}
              <p>
                {" "}
                {game.home_points} - {game.away_points}{" "}
              </p>
            </Row>
            <Row>
              <b>Date: </b>{" "}
              <p>{Moment(game.start_date).format("MMM d, y")}</p>
            </Row>
            <Row>
              <b>Season: </b> <p>{game.season_type}</p>
            </Row>
            <Row>
              <b>Stadium: </b>
              <p> {game.venue} </p>
            </Row>
          </Col>
          
          <Col>
            <iframe
              src={game.video1}
              width="570"
              height="370"
              frameborder="0"
              allowfullscreen
              title="video"
            />
          </Col>
        </Row>
        
        <Row>
          <Col>
            <div>
              <Card style={{
                width: "25rem",
                backgroundColor: "#edf0f5df"
              }}>
                <Card.Img variant="top" src={homeTeam.logos1} />
                <Card.Body>
                  <Card.Title>
                    <Link to={`../teams/${homeTeam.team_id}`}>{game.home_team}</Link>
                  </Card.Title>
                  <Card.Text>
                    <Accordion>
                      <Accordion.Item eventKey="0">
                        <Accordion.Header>Stats</Accordion.Header>
                        <Accordion.Body>
                          <table class="table">
                            <thead>
                              <tr>
                                <th>Category</th>
                                <th>Total</th>
                              </tr>
                            </thead>
                            <tbody>
                              <tr>
                                <td> Win Probability</td>
                                <td> {game.home_win_prob}</td>
                              </tr>
                              <tr>
                                <td> Pre-game Elo</td>
                                <td> {game.home_pre_elo}</td>
                              </tr>
                              <tr>
                                <td> Post-game Elo</td>
                                <td> {game.home_post_elo}</td>
                              </tr>
                            </tbody>
                          </table>
                        </Accordion.Body>
                      </Accordion.Item>
                      <Accordion.Item eventKey="1">
                        <Accordion.Header>Roster</Accordion.Header>
                        <Accordion.Body>
                          <table class="table">
                            <thead>
                              <tr>
                                <th>#</th>
                                <th>First Name</th>
                                <th>Last Name</th>
                                <th>Position</th>
                              </tr>
                            </thead>
                            <tbody>
                              {homePlayer.map((record, i) => {
                                return (
                                  <tr>
                                    <td> {record.jersey}</td>
                                    <td>
                                      {" "}
                                      <Link to={`../players/${record.player_id}`}>
                                        {record.first_name}
                                      </Link>
                                    </td>
                                    <td> {record.last_name}</td>
                                    <td> {record.position}</td>
                                  </tr>
                                );
                              })}
                            </tbody>
                          </table>
                        </Accordion.Body>
                      </Accordion.Item>
                    </Accordion>
                  </Card.Text>
                </Card.Body>
              </Card>
            </div>
          </Col>
          <Col>
            <div>
              <Card style={{ width: "25rem", backgroundColor: "#edf0f5df"}}>
                <Card.Img variant="top" src={awayTeam.logos1} />
                <Card.Body>
                  <Card.Title>
                    <Link to={`../teams/${awayTeam.team_id}`}>{game.away_team}</Link>
                  </Card.Title>
                  <Card.Text>
                    <Accordion >
                      <Accordion.Item eventKey="0">
                        <Accordion.Header>Stats</Accordion.Header>
                        <Accordion.Body>
                          <table class="table">
                            <thead>
                              <tr>
                                <th>Category</th>
                                <th>Total</th>
                              </tr>
                            </thead>
                            <tbody>
                              <tr>
                                <td> Win Probability</td>
                                <td> {game.away_win_prob}</td>
                              </tr>
                              <tr>
                                <td> Pre-game Elo</td>
                                <td> {game.away_pre_elo}</td>
                              </tr>
                              <tr>
                                <td> Post-game Elo</td>
                                <td> {game.away_post_elo}</td>
                              </tr>
                            </tbody>
                          </table>
                        </Accordion.Body>
                      </Accordion.Item>
                      <Accordion.Item eventKey="1">
                        <Accordion.Header>Roster</Accordion.Header>
                        <Accordion.Body>
                          <table class="table">
                            <thead>
                              <tr>
                                <th>#</th>
                                <th>First Name</th>
                                <th>Last Name</th>
                                <th>Position</th>
                              </tr>
                            </thead>
                            <tbody>
                              {awayPlayer.map((record, i) => {
                                return (
                                  <tr>
                                    <td> {record.jersey}</td>
                                    <td>
                                      {" "}
                                      <Link to={`../players/${record.player_id}`}>
                                        {record.first_name}
                                      </Link>
                                    </td>
                                    <td> {record.last_name}</td>
                                    <td> {record.position}</td>
                                  </tr>
                                );

                              })}
                            </tbody>
                          </table>
                        </Accordion.Body>
                      </Accordion.Item>
                    </Accordion>
                  </Card.Text>
                </Card.Body>
              </Card>
            </div>
          </Col>
        </Row>
      </Container>
    );
  };
  if (loading) {
    return <div className="App">Loading...</div>;
  }
  return <>{<ShowGame />}</>;
};

export default GameInstance;
