import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import axios from "axios";
import Paginate from "./pagination";
import {
  StringParam,
  NumberParam,
  useQueryParams,
  withDefault,
} from "use-query-params";
import Highlighter from "react-highlight-words";
import Filter from "./filter";
import Sort from "./sort";
import ModelSearch from "./modelSearch";

const TeamPage = () => {
  // const baseEndpoint = "http://localhost:5000";
  const baseEndpoint = "https://api.ncaaccess.me";
  const [team, setTeam] = useState([]);
  const [loading, setLoading] = useState(false);
  const [postsPerPage] = useState(10);

  const [params, setParams] = useQueryParams({
    page: withDefault(NumberParam, 1),
    search: withDefault(StringParam, ""),
    sort: withDefault(StringParam, ""),
    conf: withDefault(StringParam, ""),
    state: withDefault(StringParam, ""),
    city: withDefault(StringParam, ""),
  });

  useEffect(() => {
    const fetchPosts = async () => {
      setLoading(true);
      let query = ""
      if (params.conf)
        query += "&conf=" + params.conf
      if (params.state)
        query += "&state=" + params.state
      if (params.sort)
        query += "&sort=" + params.sort
      if (params.search)
        query += "&search=" + params.search 
      console.log(query)
      const res = await axios.get(baseEndpoint + "/api/teams?" + query);
      setTeam(res.data);
      setLoading(false);
    };

    fetchPosts();
  }, []);

  // Get current posts
  const indexOfLastPost = params.page * postsPerPage;
  const indexOfFirstPost = indexOfLastPost - postsPerPage;
  const currentPosts = team.slice(indexOfFirstPost, indexOfLastPost);

  // Change page
  const paginate = (pageNumber) => setParams({ ...params, page: pageNumber});


  const Posts = ({ posts, loading }) => {
    if (loading) {
      return <h2>Loading...</h2>;
    }

    return (
      <div>
        <div className="text-center text-2xl font-sans text-slate-500">
          <b>Teams</b>
        </div>
        {/* Container */}
        <div className="grid sm:grid-cols-10 md:grid-cols-5 gap-2 m-3">
          {/* Grid Item */}
          {posts.map((result) => (
            <Link
              to={`./${result.team_id}`}
              style={{
                backgroundImage: `url(${result.logos1})`,
                backgroundSize: "cover",
                display: "block",
                textDecoration: "none",
                minHeight: "150px",
                maxHeight: "550px",
                paddingBottom: "100%",
              }}
              className="shadow-lg shadow-[#160904] group container rounded-md flex justify-center items-center mx-auto content-div"
              key={result.team_id}
            >
              {/* Hover Effects */}
              <div
                style={{
                  textAlign: "center",
                  verticalAlign: "middle",
                }}
                className="opacity-0 group-hover:opacity-100 text-center py-10"
              >
                <b> School </b>
                <p> <Highlighter autoEscape={true} textToHighlight={result.school_name} searchWords={params.search.split(/[%20 \s,;/]+/)}/> </p>
                <b> City </b>
                <p> <Highlighter autoEscape={true} textToHighlight={result.city} searchWords={params.search.split(/[%20 \s,;/]+/)}/> </p>
                <b> State </b>
                <p> <Highlighter autoEscape={true} textToHighlight={result.state_name} searchWords={params.search.split(/[%20 \s,;/]+/)}/> </p>
                <b> Conference </b>
                <p> <Highlighter autoEscape={true} textToHighlight={result.conference} searchWords={params.search.split(/[%20 \s,;/]+/)}/> </p>
              </div>
            </Link>
          ))}
        </div>
      </div>
    );
  };

  return (
    <div className="container mt-5" style={{backgroundColor: "#edf0f5"}}>
      <ModelSearch></ModelSearch>
      <Filter
        filter={["State", "Conf"]}
        filterOptions={[["AL", "AZ", "AR", "CA", "CO", "CT", "FL", "GA", 
        "ID", "IL", "IN", "IA", "KS", "KY", "LA", "MD", 
        "MA", "MI", "MN", "MS", "MO", "NE", "NV", "NJ", 
        "NM", "NY", "NC", "OH", "OK", "OR", "PA", "SC", 
        "TN", "TX", "UT", "VT", "VA", "WA", "WV", "WI", "WY"], ["Big 12","Mid-American", "Mountain West", "Pac-12", "American Athletic", "Sun Belt", "Big Ten", "FBS Independents", "Conference USA", "SEC", "ACC"]]}
      />
      <Sort SortOptions={["School", "City"]}
            SortNames={["school_name", "city"]}/>
      <Posts posts={currentPosts} loading={loading} />
      <center>
        <Paginate
          postsPerPage={postsPerPage}
          totalPosts={team.length}
          paginate={paginate}
          currentPage={params.page}
        />
        <div>Total Results = {team.length}</div>
      </center>
    </div>
  );
};

export default TeamPage;
