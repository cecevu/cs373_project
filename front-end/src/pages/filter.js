import React from "react";
import { Dropdown, DropdownButton, ButtonGroup } from "react-bootstrap";
import { useLocation } from "react-router-dom";
import { StringParam, useQueryParams, withDefault } from "use-query-params";
import "./dropdown.css"

const Filter = ({ filter, filterOptions }) => {
  let categories = new Map();
  filter.forEach((cat) =>
    categories.set(cat, withDefault(StringParam, ""))
  );

  const [params, setParams] = useQueryParams(categories);
  let location = useLocation();

  return (
    <>
      <div
        className={`search filter`}
        style={{
          textAlign: "center",
          verticalAlign: "middle",
          color: "black",
        }}
      >
        {filter.map((category, idx) => {
          const option = filterOptions[idx];
          return (
            <DropdownButton
              key={idx}
              className="item"
              as={ButtonGroup}
              variant="outline-primary" 
              title={`Filter by ${category}`}
              style={{
                textAlign: "center",
                verticalAlign: "middle",
                color: "black",
              }}
            >
              {option.map((i, j) => {
                return (
                  <Dropdown.Item
                    key={j}
                    onClick={() => {setParams({page: 1,[category.toString().toLowerCase()] : i.toString() }); window.location.href = window.location.href}}
                  >
                    {`${i}`}
                  </Dropdown.Item>
                );
              })}
              <Dropdown.Item
                    key={option.length + 1}
                    onClick={() => {setParams({page: 1, [category.toString().toLowerCase()] : "" }); window.location.href = window.location.href}}
                  >
                    No Filter
                  </Dropdown.Item>
            </DropdownButton>
          );
        })}
      </div>
    </>
  );
};

export default Filter;
