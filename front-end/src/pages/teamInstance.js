import React, { useState, useEffect } from "react";
import { useParams } from "react-router-dom";
import axios from "axios";
import { Link } from "react-router-dom";
import Image from "react-bootstrap/Image";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Accordion from "react-bootstrap/Accordion";
import Button from "react-bootstrap/Button";
import { TwitterTimelineEmbed } from "react-twitter-embed";
import stadium from '../pictures/stadium.jpg';

const TeamInstance = () => {
  const { id } = useParams();
  // const baseEndpoint = "http://localhost:5000";
  const baseEndpoint = "https://api.ncaaccess.me"
  const endpoint = baseEndpoint + "/api/teams/";
  // const endpoint = 'http://localhost:5000/api/teams/1';
  const [team, setTeam] = useState([]);
  const [game, setGame] = useState([]);
  const [player, setPlayer] = useState([]);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    axios
      .get(endpoint + id)
      .then((res) => {
        setTeam(res.data);
        setLoading(false);
      })
      .catch((e) => console.log(e));

    axios
      .get(baseEndpoint + "/api/games")
      .then((res) => {
        setGame(res.data);
      })
      .catch((e) => console.log(e));
    axios
      .get(baseEndpoint + "/api/players")
      .then((res) => {
        setPlayer(res.data);
      })
      .catch((e) => console.log(e));
  }, [endpoint, id]);

  var game2022List = [];
  var game2021List = [];

  for (var i = 0; i < game.length; i++) {
    if (
      game[i].away_team === team.school_name ||
      game[i].home_team === team.school_name
    ) {
      if (game[i].season === 2021) {
        game2021List.push(game[i]);
      } else {
        game2022List.push(game[i]);
      }
    }
  }

  game2022List.sort(function (a, b) {
    return a.week - b.week;
  });

  game2021List.sort(function (a, b) {
    return a.week - b.week;
  });

  var playerList = [];
  var player2022List = [];
  var player2021List = [];
  for (i = 0; i < player.length; i++) {
    if (player[i].team === team.school_name) {
      if (player[i].season === 2021) {
        player2021List.push(player[i]);
      } else {
        player2022List.push(player[i]);
      }
      playerList.push(player[i]);
    }
  }
  if (team === undefined) return null;

  const ShowTeam = () => {
    return (
      <Container style={{backgroundSize: "cover", backgroundImage: `url(${stadium})`}}>
        <center>
          <Row>
            <Col>
              <Image src={team.logos1} rounded />
            </Col>
            <Col>
              <p></p>
              <b> School </b>
              <p> {team.school_name} </p>
              <b> Mascot </b>
              <p> {team.mascot} </p>
              <b> Location </b>
              <p>
                {" "}
                {team.city}, {team.state_name}{" "}
              </p>
              <b> Stadium </b>
              <p> {team.venue_name} </p>
              <b> Conference </b>
              <p> {team.conference} </p>
            </Col>
            <Col>
              <TwitterTimelineEmbed
                sourceType="profile"
                screenName={team === undefined ? "ncaa" : team.twitter.slice(1)}
                options={{ height: 350 }}
              />
            </Col>
          </Row>
        </center>

        <center style={{
          borderRadius: "5px",
          padding: "5px",
          backgroundColor: "#edf0f5df"
        }}>
        <h1> Games </h1>
        <Accordion>
          <Accordion.Item eventKey="0">
            <Accordion.Header>
              <Button variant="outline-primary"> 2022 Schedule and Results</Button>
            </Accordion.Header>
            <Accordion.Body>
              <table class="table">
                <thead>
                  <tr>
                    <th>Week</th>
                    <th>Home Team</th>
                    <th>Away Team</th>
                    <th>Home Points</th>
                    <th>Away Points</th>
                  </tr>
                </thead>
                <tbody>
                  {game2022List.map((record, i) => {
                    return (
                      <tr>
                        <td>
                          {" "}
                          <Link to={`../games/${record.game_id}`}>
                            {record.week}
                          </Link>
                        </td>
                        <td>
                          {" "}
                          <Link to={`../games/${record.game_id}`}>
                            {record.home_team}
                          </Link>
                        </td>
                        <td> {record.away_team}</td>
                        <td> {record.home_points}</td>
                        <td> {record.away_points}</td>
                      </tr>
                    );
                  })}
                </tbody>
              </table>
            </Accordion.Body>
          </Accordion.Item>
          <Accordion.Item eventKey="1">
            <Accordion.Header>
              <Button variant="outline-primary"> 2021 Schedule and Results</Button>
            </Accordion.Header>
            <Accordion.Body>
              <table class="table">
                <thead>
                  <tr>
                    <th>Week</th>
                    <th>Home Team</th>
                    <th>Away Team</th>
                    <th>Home Points</th>
                    <th>Away Points</th>
                  </tr>
                </thead>
                <tbody>
                  {game2021List.map((record, i) => {
                    return (
                      <tr>
                        <td>
                          {" "}
                          <Link to={`../games/${record.game_id}`}>
                            {record.week}
                          </Link>
                        </td>
                        <td>
                          {" "}
                          <Link to={`../games/${record.game_id}`}>
                            {record.home_team}
                          </Link>
                        </td>
                        <td> {record.away_team}</td>
                        <td> {record.home_points}</td>
                        <td> {record.away_points}</td>
                      </tr>
                    );
                  })}
                </tbody>
              </table>
            </Accordion.Body>
          </Accordion.Item>
        </Accordion>
        
        <h1> Players </h1>
        <Accordion>
          <Accordion.Item eventKey="0">
            <Accordion.Header>
              {" "}
              <Button variant="outline-primary"> 2022 Roster</Button>{" "}
            </Accordion.Header>
            <Accordion.Body>
              <table class="table">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Position</th>
                  </tr>
                </thead>
                <tbody>
                  {player2022List.map((record, i) => {
                    return (
                      <tr>
                        <td> {record.jersey}</td>
                        <td>
                          {" "}
                          <Link to={`../players/${record.player_id}`}>
                            {record.first_name}
                          </Link>
                        </td>
                        <td> {record.last_name}</td>
                        <td> {record.position}</td>
                      </tr>
                    );
                  })}
                </tbody>
              </table>
            </Accordion.Body>
          </Accordion.Item>
          <Accordion.Item eventKey="1">
            <Accordion.Header>
              {" "}
              <Button variant="outline-primary"> 2021 Roster</Button>{" "}
            </Accordion.Header>
            <Accordion.Body>
              <table class="table">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Position</th>
                  </tr>
                </thead>
                <tbody>
                  {player2021List.map((record, i) => {
                    return (
                      <tr>
                        <td> {record.jersey}</td>
                        <td>
                          {" "}
                          <Link to={`../players/${record.player_id}`}>
                            {record.first_name}
                          </Link>
                        </td>
                        <td> {record.last_name}</td>
                        <td> {record.position}</td>
                      </tr>
                    );
                  })}
                </tbody>
              </table>
            </Accordion.Body>
          </Accordion.Item>
        </Accordion>
        </center>
      </Container>
    );
  };

  if (loading) {
    return <div className="App">Loading...</div>;
  }

  return <>{<ShowTeam />}</>;
};

export default TeamInstance;
