import React from "react";
import { Pagination } from "react-bootstrap";

const Paginate = ({ postsPerPage, totalPosts, paginate, currentPage }) => {
  const paginationRange = [];

  for (let i = 1; i <= Math.ceil(totalPosts / postsPerPage); i++) {
    paginationRange.push(i);
  }

  const siblingCount = 4;

  // const paginationRange = Math.ceil(totalPosts / postsPerPage)

  const onNext = () => {
    paginate(currentPage + 1);
  };

  const onPrevious = () => {
    paginate(currentPage - 1);
  };

  let lastPage = paginationRange[paginationRange.length - 1];
  let items = [];

  items.push(<Pagination.First onClick={() => paginate(1)} />);

  if (currentPage > 1) {
    items.push(<Pagination.Prev onClick={() => onPrevious()} />);
  } else {
    items.push(<Pagination.Prev disabled />);
  }

  items.push(
    <Pagination.Item
      key={1}
      active={1 === currentPage}
      activeLabel=""
      onClick={() => paginate(1)}
    >
      {1}
    </Pagination.Item>
  );

  if (currentPage > siblingCount + 2) {
    items.push(<Pagination.Ellipsis />);
  }

  for (
    let i = Math.max(currentPage - siblingCount, 2);
    i <= Math.min(currentPage + siblingCount, lastPage - 1);
    i++
  ) {
    items.push(
      <Pagination.Item
        key={i}
        active={i === currentPage}
        activeLabel=""
        onClick={() => paginate(i)}
      >
        {i}
      </Pagination.Item>
    );
  }

  if (currentPage < lastPage - siblingCount - 1) {
    items.push(<Pagination.Ellipsis label="" />);
  }

  items.push(
    <Pagination.Item
      key={lastPage}
      active={lastPage === currentPage}
      activeLabel=""
      onClick={() => paginate(lastPage)}
    >
      {lastPage}
    </Pagination.Item>
  );

  if (currentPage < lastPage) {
    items.push(<Pagination.Next onClick={() => onNext()} />);
  } else {
    items.push(<Pagination.Next disabled />);
  }

  items.push(<Pagination.Last onClick={() => paginate(lastPage)} />);

  return (
    <div>
      <center>
        <Pagination size="sm">{items}</Pagination>
      </center>
    </div>
  );
};
export default Paginate;
