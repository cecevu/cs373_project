import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import axios from "axios";
import Paginate from "./pagination";
import Moment from "moment";
import {
  Grid,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Paper,
} from "@mui/material";
import {
  StringParam,
  NumberParam,
  useQueryParams,
  withDefault,
} from "use-query-params";
import Highlighter from "react-highlight-words";
import Filter from "./filter";
import Sort from "./sort";
import ModelSearch from "./modelSearch";

const GamePage = () => {

  const [params, setParams] = useQueryParams({
    page: withDefault(NumberParam, 1),
    search: withDefault(StringParam, ""),
    sort: withDefault(StringParam, ""),
    hometeam: withDefault(StringParam, ""),
    awayteam: withDefault(StringParam, ""),
    season: withDefault(NumberParam, null),
  });

  const [game, setGame] = useState([]);
  const [loading, setLoading] = useState(false);
  const [currentPage, setCurrentPage] = useState(1);
  const [postsPerPage] = useState(20);
  // const baseEndpoint = "http://localhost:5000";
  const baseEndpoint = "https://api.ncaaccess.me";

  useEffect(() => {
    const fetchPosts = async () => {
      setLoading(true);
      let query = ""
      if (params.hometeam)
        query += "&hteam=" + params.hometeam
      if (params.awayteam)
        query += "&ateam=" + params.awayteam
      if (params.season)
        query += "&season=" + params.season
      if (params.sort)
        query += "&sort=" + params.sort
      if (params.search)
        query += "&search=" + params.search 
      console.log(query)

      const res = await axios.get(baseEndpoint + "/api/games?" + query);
      setGame(res.data);
      setLoading(false);
    };

    fetchPosts();
  }, []);

  // Get current posts
  const indexOfLastPost = params.page * postsPerPage;
  const indexOfFirstPost = indexOfLastPost - postsPerPage;
  const currentPosts = game.slice(indexOfFirstPost, indexOfLastPost);

  // Change page
  const paginate = (pageNumber) => setCurrentPage(pageNumber);

  const Posts = ({ posts, loading }) => {
    if (loading) {
      return <h2>Loading...</h2>;
    }

    return (
      <div>
        <div className="text-center text-2xl font-sans text-slate-500">
          <b>Games</b>
        </div>
        <Grid container justifyContent="center">
          <TableContainer component={Paper} style={{ width: "80%" }}>
            <Table component="div" sx={{ minWidth: 650 }}>
              <TableHead component="div">
                <TableRow component="div">
                  <TableCell component="div" align="center">
                    Home Team
                  </TableCell>
                  <TableCell component="div" align="center">
                    Away Team
                  </TableCell>
                  <TableCell component="div" align="center">
                    Date
                  </TableCell>
                  <TableCell component="div" align="center">
                    Winner
                  </TableCell>
                  <TableCell component="div" align="center">
                    Home Score
                  </TableCell>
                  <TableCell component="div" align="center">
                    Away Score
                  </TableCell>
                  <TableCell component="div" align="center">
                    season
                  </TableCell>
                </TableRow>
              </TableHead>
              <TableBody component="div">
                {posts.map((result) => (
                  <TableRow
                    hover
                    key={result.game_id}
                    component={Link}
                    to={`./${result.game_id}`}
                    style={{ textDecoration: "none" }}
                  >
                    <TableCell component="div" scope="center">
                    <Highlighter autoEscape={true} textToHighlight={result.home_team} searchWords={params.search.split(/[%20 \s,;/]+/)}/>
                    </TableCell>
                    <TableCell component="div" align="center">
                    <Highlighter autoEscape={true} textToHighlight={result.away_team} searchWords={params.search.split(/[%20 \s,;/]+/)}/>
                    </TableCell>
                    <TableCell component="div" align="center">
                      {Moment(result.start_date).format("MMM d, y")}
                    </TableCell>
                    <TableCell component="div" align="center">
                      {result.home_points > result.away_points
                        ? result.home_team
                        : result.away_team}
                    </TableCell>
                    <TableCell component="div" align="center">
                      {result.home_points}
                    </TableCell>
                    <TableCell component="div" align="center">
                      {result.away_points}
                    </TableCell>
                    <TableCell component="div" align="center">
                    {result.season}
                    </TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </TableContainer>
        </Grid>
      </div>
    );
  };

  return (
    <div className="container mt-5" style={{backgroundColor: "#edf0f5"}}>
      <ModelSearch></ModelSearch>
      <Filter
        filter={["HomeTeam", "AwayTeam", "Season"]}
        filterOptions={[["Air Force", "Akron", "Alabama", "Appalachian State", "Arizona", "Arizona State",
         "Arkansas", "Arkansas State", "Army", "Auburn", "Ball State", "Baylor", "Boise State", "Boston College",
         "Bowling Green", "Buffalo", "BYU", "California", "Central Michigan", "Charlotte", "Cincinnati", "Clemson",
        "Coastal Carolina", "Colorado", "Colorado State", "Duke", "East Carolina", "Eastern Michigan", "Florida Atlantic",
        "FIU", "Florida", "Florida State", "Fresno State", "Georgia", "Georgia Southern", "Georgia State", "Georgia Tech",
        "Hawaii", "Houston", "Illinois", "Indiana", "Iowa", "Iowa State", "James Madison", "Kansas", "Kansas State",
        "Kent State", "Kentucky", "Liberty", "Louisiana", "Louisiana–Monroe", "Louisiana Tech", "Louisville", "LSU",
        "Marshall", "Maryland", "Memphis", "Miami", "Miami", "Michigan", "Michigan State", "Middle Tennessee",
        "Minnesota", "Mississippi State", "Missouri", "Navy", "NC State", "Nebraska", "Nevada", "New Mexico",
        "New Mexico State", "North Carolina", "North Texas", "Northern Illinois", "Northwestern", "Notre Dame",
        "Ohio", "Ohio State", "Oklahoma", "Oklahoma State", "Old Dominion", "Ole Miss", "Oregon", "Oregon State",
        "Penn State", "Pittsburgh", "Purdue", "Rice", "Rutgers", "San Diego State", "San Jose State", "SMU",
        "South Alabama", "South Carolina", "South Florida", "Southern Miss", "Stanford", "Syracuse", "TCU", "Temple",
        "Tennessee", "Texas", "Texas A&M", "Texas State", "Texas Tech", "Toledo", "Troy", "Tulane", "Tulsa", "UAB", "UCF",
        "UCLA", "UConn", "UMass", "UNLV", "USC", "UTEP", "UTSA", "Utah", "Utah State", "Vanderbilt", "Virginia",
        "Virginia Tech", "Wake Forest", "Washington", "Washington State", "West Virginia", "Western Kentucky",
        "Western Michigan", "Wisconsin", "Wyoming"], 
        ["Air Force", "Akron", "Alabama", "Appalachian State", "Arizona", "Arizona State",
        "Arkansas", "Arkansas State", "Army", "Auburn", "Ball State", "Baylor", "Boise State", "Boston College",
        "Bowling Green", "Buffalo", "BYU", "California", "Central Michigan", "Charlotte", "Cincinnati", "Clemson",
       "Coastal Carolina", "Colorado", "Colorado State", "Duke", "East Carolina", "Eastern Michigan", "Florida Atlantic",
       "FIU", "Florida", "Florida State", "Fresno State", "Georgia", "Georgia Southern", "Georgia State", "Georgia Tech",
       "Hawaii", "Houston", "Illinois", "Indiana", "Iowa", "Iowa State", "James Madison", "Kansas", "Kansas State",
       "Kent State", "Kentucky", "Liberty", "Louisiana", "Louisiana–Monroe", "Louisiana Tech", "Louisville", "LSU",
       "Marshall", "Maryland", "Memphis", "Miami", "Miami", "Michigan", "Michigan State", "Middle Tennessee",
       "Minnesota", "Mississippi State", "Missouri", "Navy", "NC State", "Nebraska", "Nevada", "New Mexico",
       "New Mexico State", "North Carolina", "North Texas", "Northern Illinois", "Northwestern", "Notre Dame",
       "Ohio", "Ohio State", "Oklahoma", "Oklahoma State", "Old Dominion", "Ole Miss", "Oregon", "Oregon State",
       "Penn State", "Pittsburgh", "Purdue", "Rice", "Rutgers", "San Diego State", "San Jose State", "SMU",
       "South Alabama", "South Carolina", "South Florida", "Southern Miss", "Stanford", "Syracuse", "TCU", "Temple",
       "Tennessee", "Texas", "Texas A&M", "Texas State", "Texas Tech", "Toledo", "Troy", "Tulane", "Tulsa", "UAB", "UCF",
       "UCLA", "UConn", "UMass", "UNLV", "USC", "UTEP", "UTSA", "Utah", "Utah State", "Vanderbilt", "Virginia",
       "Virginia Tech", "Wake Forest", "Washington", "Washington State", "West Virginia", "Western Kentucky",
       "Western Michigan", "Wisconsin", "Wyoming"], 
        [2022, 2021]]}
      />
      <Sort SortOptions={["Date", "Home Team", "Away Team"]} SortNames={["start_date", "home_team", "away_team"]} />
      <Posts posts={currentPosts} loading={loading} />
      <center>
        <Paginate
          postsPerPage={postsPerPage}
          totalPosts={game.length}
          paginate={paginate}
          currentPage={currentPage}
        />
        <div>Total Results = {game.length}</div>
      </center>
    </div>
  );
};

export default GamePage;
