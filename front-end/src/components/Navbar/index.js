import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import NavDropdown from 'react-bootstrap/NavDropdown';

function NavBar() {
  return (
    <Navbar bg="light" expand="lg">
      <Container>
        <Navbar.Brand href="/">NCAAccess</Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="me-auto">
            <Nav.Link href="/" >Home</Nav.Link>
            <Nav.Link href="/about">About</Nav.Link>
            <Nav.Link href="/search">Search</Nav.Link>
            <Nav.Link href="/visuals">Visualizations</Nav.Link>
            <Nav.Link href="/pvisuals">Provider Visualizations</Nav.Link>
            <NavDropdown title="Models" id="basic-nav-dropdown">
              <NavDropdown.Item href="/teams">Teams</NavDropdown.Item>
              <NavDropdown.Item href="/players">Players</NavDropdown.Item>
              <NavDropdown.Item href="/games">Games</NavDropdown.Item>
            </NavDropdown>
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
}

export default NavBar;




// import React from "react";
// import { Nav, NavLink, NavMenu } 
//     from "./NavbarElements";
  
// const Navbar = () => {
//   return (
//     <>
//       <Nav>
//         <NavMenu>
//         <NavLink to="/" activeStyle>
//           <button type="button" class="btn btn-warning">Home</button>
//           </NavLink>
//           <NavLink to="/about" activeStyle>
//             <button type="button" class="btn btn-warning">About</button>
//           </NavLink>
//           <NavLink to="/teams" activeStyle>
//             <button type="button" class="btn btn-warning">Teams</button>
//           </NavLink>
//           <NavLink to="/players" activeStyle>
//             <button type="button" class="btn btn-warning">Players</button>
//           </NavLink>
//           <NavLink to="/games" activeStyle>
//             <button type="button" class="btn btn-warning">Games</button>
//           </NavLink>
//         </NavMenu>
//       </Nav>
//     </>
//   );
// };
  
// export default Navbar;