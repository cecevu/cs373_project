import * as renderer from "react-test-renderer";
import NavBar from "./components/Navbar";
import Paginate from "./pages/pagination";
import App from "./App";
import { render, screen, waitFor } from "@testing-library/react";
import TeamPage from "./pages/teamPage";
import { MemoryRouter, BrowserRouter } from "react-router-dom";
import About from "./pages/about";
import PlayerPage from "./pages/playerPage";
import GamePage from "./pages/gamePage";
import Filter from "./pages/filter";
import Sort from "./pages/sort";
import { QueryParamProvider } from "use-query-params";
import { ReactRouter6Adapter } from "use-query-params/adapters/react-router-6";

// Test 1
test("Game Card rendered", async () => {
  render(<App />);
  const linkElement = screen.getByText("Games");
  expect(linkElement).toBeInTheDocument();
});

// Test 2
test("Team Card rendered", async () => {
  render(<App />);
  const linkElement = screen.getByText("Teams");
  expect(linkElement).toBeInTheDocument();
});

// Test 3
test("Player Card rendered", async () => {
  render(<App />);
  const linkElement = screen.getByText("Players");
  expect(linkElement).toBeInTheDocument();
});

// Test 4
test("Navbar", async () => {
  const tree = renderer.create(<NavBar />).toJSON();
  expect(tree).toMatchSnapshot();
});

// Test 5
test("Pagination", async () => {
  const tree = renderer.create(<Paginate />).toJSON();
  expect(tree).toMatchSnapshot();
});

// Test 6
test("About", async () => {
  render(<About />);
  const linkElement = screen.getByText("About");
  expect(linkElement).toBeInTheDocument();
});

// Test 7
test("TeamPage", async () => {
  const element = render(
    <MemoryRouter>
      <QueryParamProvider adapter={ReactRouter6Adapter}>
        <TeamPage />
      </QueryParamProvider>
    </MemoryRouter>
  );
  expect(element).toMatchSnapshot();
});

// Test 8
test("GamePage", async () => {
  const element = render(
    <MemoryRouter>
      <QueryParamProvider adapter={ReactRouter6Adapter}>
        <GamePage />
      </QueryParamProvider>
    </MemoryRouter>
  );
  expect(element).toMatchSnapshot();
});

// Test 9
test("PlayerPage", async () => {
  const element = render(
    <MemoryRouter>
      <QueryParamProvider adapter={ReactRouter6Adapter}>
        <PlayerPage />
      </QueryParamProvider>
    </MemoryRouter>
  );
  expect(element).toMatchSnapshot();
});

// Test 10
test("Index", () => {
  const element = render(<App />);
  expect(element).toMatchSnapshot();
});

// Test 11
test("Filter", async () => {
  const element = render(
    <MemoryRouter>
      <QueryParamProvider adapter={ReactRouter6Adapter}>
        <Filter filter={["State"]} filterOptions={[["TX"]]} />
      </QueryParamProvider>
    </MemoryRouter>
  );
  expect(element).toMatchSnapshot();
});

test("Sort", async () => {
  const element = render(
    <MemoryRouter>
      <QueryParamProvider adapter={ReactRouter6Adapter}>
        <Sort SortOptions={["State"]} SortNames={["state"]} />
      </QueryParamProvider>
    </MemoryRouter>
  );
  
  expect(element).toMatchSnapshot();
});
