import './App.css';
import NavBar from './components/Navbar';
import { BrowserRouter as Router, Routes, Route}
    from 'react-router-dom';
import { QueryParamProvider } from 'use-query-params';
import { ReactRouter6Adapter } from 'use-query-params/adapters/react-router-6';


import Home from './pages';
import About from './pages/about';

import TeamInstance from './pages/teamInstance';
import Game from './pages/gameInstance';
import PlayerInstance from './pages/playerInstance';
import TeamPage from './pages/teamPage';
import GamePage from './pages/gamePage';
import PlayerPage from './pages/playerPage';
import Search from './pages/search';
import VisPage from './pages/visPage';
import PVisPage from './pages/provVisPage';

function App() {
  return (
    <Router>
    <QueryParamProvider adapter={ReactRouter6Adapter}>
    <NavBar />
    <Routes>
        <Route exact path='/' element={<Home />} />
        <Route path='/games/:id' element={<Game />} />
        <Route path='/teams/:id' element={<TeamInstance />} />
        <Route path='/players/:id' element={<PlayerInstance />} />
        <Route path='/teams' element={<TeamPage />} />
        <Route path='/games' element={<GamePage/>} />
        <Route path='/players' element={<PlayerPage/>} />
        <Route path='/about' element={<About/>} />
        <Route path='/search' element={<Search/>} />
        <Route path='/visuals' element={<VisPage/>} />
        <Route path='/pvisuals' element={<PVisPage/>} />
    </Routes>
    </QueryParamProvider>
    </Router>
  );
}

export default App;
